5   Use of Markdown
5   Suitably separate documents (SVG, XML)
4   Describing your client (NED)
3   Describing your application domain (NED)
4   Problem trying to solve
4   Vision for product
4   List of system stakeholders
4   Description of stakeholders
4   Use-case diagram (PTD, BLL)
4   Key actors
4   Primary functional goals
4   Consideration of non-human actors
4   Goal steps (IMPLICIT)
4   Alternative flow
4   Exceptional flow
5   Functional requirements (Bit PTL)
4   Non-Functional requirements
4   Atomicness of requirements
4   Clear & simple requirement structure (NID)

5   High-level architecture diagram
5   Relevant external systems
4   Explanation of components
4   Static UML modelling aspect
4   Dynamic UML modelling aspect (Added sequence UML, I think add a another UML diagram for a 5)
4   Model context
4   The motivation for choice
4   A reflection on the modelling choices
4   Knowledge gained from models
5   Description of Test strategy
5   Testing frameworks to be used
4   Challenges for testability
4   Overcoming testability challenges

+Update tense and wording in Design to make sound more like it's all fininished now.
+ remove emojis from BPMN
+crows foot diagram and metion of second sprint in design






MD links broken

Architecture diagram very effective and well described. Well done.
CD slightly unexciting. Not sure how much insight you gained from that after the ER was there.

Success story with Apiary is fantastic. 

Testing:
Good strategies based on leveraging powerful tools and processes. Well done.


Evaluation:
Description of evaluation approach
Justification of aproach used
Description of users and environment
descrition of feedback
How feedback was used to refine final iteration
Samples of evaluation materials

