# Feedback and actions from evaluation tasks

## Evaluation session 1

* Searching by location won't show existing books if not in your location but available elsewhere
* Email not sending properly
* Emails should be prefilled when logged in (eg. when creating requests, reviews)
* My books could be more visible 
* Check in is nto intuative naming
* Request copy button could stand out more (request for your location) x2
* Quantity field not showing numbers on mobile until losing focus?
* Barcode SVG doesn't look like a barcode - glitched out SVG?
* ISBN - remove dashes, autofetch showing errors while typing in ISBN
* Request created - should be "requested"
* Request update button should be "update email" so it's clear what it updates
* Behaviour of rejected status on requests - confusing/bugged
* Delegate email - default should be configurable

## Evaluation session 2
* Barcode wrong on some books - laptop camera bad or bug?
* Not clear why can't add book when no location given (needs clearer validation feedback)
* Not necessarily clear you need to click Update Status button on requests modal × 2
* Use coloured badges for status
* Pressing enter on the delete search bar reloads the page × 2
* Difference between deleting books and copies maybe not totally clear - perhaps a message warning that delete book deletes the whole book (all copies) × 2
* Maybe put search by barcode button below the field so it doesn't look like it is a general search button
* Allow sorting the overdue copies table?
* Feels like you should be able to click on more table rows 
* Delete all completed/requested doesn't always show that they have been deleted on first click?
* Maybe make it more clear that you can click requests in the table - even just text explaining that
* Search in delete showing copies that it shouldn't sometimes - "delete me" showing as if unfiltered
* Should you necessarily need an author? Some books kind of don't have one
* Confirmation for deleting a book so mistakes don't happen
* Make the fact you can add more authors clearer maybe? (There's already text, not sure)
* Maybe add links to delete/modify a book in the user view of books?
* Link to see the book after you create it
* Delete book/copy page in general seems a little unclear - people don't seem too sure about having to do the search thing on it - should probably be a table like the other views
