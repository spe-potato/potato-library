# Evaluation Tasks

Welcome to our user evaluation. We're going to ask you to complete a few tasks with our new web app so we can work out what we need to improve in later releases. Depending on what role you feel you have, try completing the relevant tasks.

## User Tasks

You just read an article linked on Hacker News about the benefits of reading more books. Apparently Bill Gates reads 50 books a year! Excited by this new information you decide to dive right in and borrow a book from the wonderful Potato library straight away! You head over to the shelf and pick a book that sounds interesting:

Task 1: Pick a book from the collection on the table.

Wow, that sounds exciting! Now to let your colleagues know and to keep track of which books you are borrowing, you need to check out the book using the new and exciting Potato Library Web App!

Task 2: Find your book and take out a copy. You could even borrow a few since you're such an enthusiastic reader.

Well done! You also read an article about speed reading on Hacker News. Since you're such a good speed reader, you already finished that book. Impressive!

Task 3: Go back to the home page of the app. Find the book(s) you have borrowed and return the book you just finished.

Awesome job! You're on Hacker News. Again. This time you saw an article about Python. What an interesting read! The article mentioned a book called 'Python Pocket Reference'.

Task 4: Check if this book is available in your potato library.

Oh no! that book isn't available in Bristol! You'll have to go to London to pick it up. Only joking! You can create a request for that book and hopefully your admin staff will get one for you.

Task 5: Create a request for the book you just tried looking for.

Nice! Hopefully that'll come through soon and you'll be able to learn some more about Python


# Admin Tasks

It turns out an article about the benefits of reading books has been making the rounds in Potato. All the employees are very excited to read more and the library is seeing a lot of use.

Task 1: Check the status of things by going into admin view.

Cool! You can see all the overdue copies and requests.

Potato is expanding and so are it's bookshelves. A new book has been ordered.

Task 2: Add this book to your collection

Lots of employees have been reading some article about an exciting programming language and really want to read a book that isn't available in Bristol.

Task 3: Check the requests page and 'order' the book that has been requested (ask one of us). Add the book to the collection

Nice job! Peace has been restored to the world once again and Potato employees can now learn Python at last!
