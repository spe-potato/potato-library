# Requirements

At the beginning of the project, as a team along with the client, we collected a number of requirements to understand exactly what it was we were building. Over the following sprints we worked on completing backlog items based on these requirements to make sure the final product delivered to the client was what they were looking for.

## Stakeholders:
* Potato employees
    * Borrowers: These are the main app users. They are Potato staff members, spread across 3 different offices, looking to browse, borrow, and return books.
    * Office Managers and the Admins: These are studio managers or other potato staff members who keep the library up to date, purchase new books and learn how the library is being used
    * Potato developers: They might want to use our Library API to build their own apps on top of it or to integrate the Library with other in-office services.
    * Book vendors: Companies that would have their products pointed to when a request for a missing book is made, for example, Amazon.


## Use cases and user stories
A high-level Use Case diagram gives a general overview of how the application we are developing should work.

![Use case Diagram](images/use-case.png "Use case diagram")

We can trace different flows:

* Users taking out books: The user searches for a book -> The user finds the given copies in their location -> Signs in with email address -> Checks out a book.
* Users Returning book: The user signs in -> Finds a copy in their borrowed books -> Checks in a book.
* Admin adding to collection: Admin signs into admin dashboard -> finds a list of all requested books-> Orders them from an online retailer through the app -> Receives books -> Adds book once received through the dashboard.

Alternative flows:

* User can't find the book they are looking for: User searches book -> There are no copies of the book -> User fills out the request form -> Requests are then sent to the Admin to be handled.

Exceptional flows:

* User looks to see if the book exists in the collection but doesn't follow through with checkout: User Searches book -> They view the copies -> leave the application.

For a more detailed overview of the application, we constructed a BPMN diagram that shows all the use case goals we expect to be present in the second version of the Library App.

![BPML](images/bpmn.png "BPMN")

During our first Sprint Planning meeting with the client, together we came up with a number of user stories that describe how the product works from the perspective of the user and administrators on a high level. User stories on their own can sometimes be vague and it might not be clear when exactly a user story should be considered complete. Following some further consultation with Potato, we added acceptance criteria, so that it is very clear when all the requirements have been met.

Together, the user stories and their acceptance criteria form a set of functional requirements that the Library must meet. Progress of each story was tracked though OpenProject's TaskBoard (can be found in a menu in the backlogs section). There was also a need for the clients to be able to see how much progress has been made, so as a result we also updated a Trello board of completed stories. Towards the end of the project we focused more heavily on using the Trello board as it was easier for us to keep the client up to date that way and integrate the plans for the retro sessions.

### Stories for the first sprint

The aim of the first sprint was to develop the key user-side components of the Library. The following user stories represent requirements for the MVP release:


* FIND-1.0 **As a user** I want a list of books *so that* I know what we have available.

    * FIND-1.0.0 On the main page of the app, all the books in the database should be shown in a list.
    * FIND-1.0.1 For each book the user should be able to select it and see in which locations it is available and see if it is on loan or not.


* FIND-1.1 **As a user** I'd like to search by ISBN number *so* I can find the right book.


* FIND-1.2 **As a user** I'd like to filter books by location *so* that I know which ones are available in my studio.


* BORROW-1.0 **As a user** I'd like to fill in a form to check out a book.

    * BORROW-1.0.1 The lending table has to be correctly updated.
    * BORROW-1.0.2 The user should only have to enter their email and press a single button to check a copy out.
    * BORROW-1.0.3 The app must update the 'availability' field to reflect user's actions.


* BORROW-1.1 **As a user** I'd like to check in a book quickly so that I do not put it off and forget about it.

    * BORROW-1.1.0 The lending table has to be correctly updated.
    * BORROW-1.1.1 Checking in a book should only take a single click.
    * BORROW-1.1.2 The app must update the 'availability' field to reflect user's actions.


* MANAGE-1.0 **As a user** I'd like to put my name in *so that* I can view my books and check out new ones.

    * MANAGE-1.0.0 This should be implemented as user accounts.
    * MANAGE-1.0.2 Per Potato's request, no passwords should be required as this will be an internal system.
    * MANAGE-1.0.3 Each user needs to have a dashboard with a list of the books they have taken out or reserved.
    * MANAGE-1.0.4 This dashboard has to be accessible in one click from the Library's home page.
    * MANAGE-1.0.5 The dashboard could even be visible as soon as a user logs in.


* MANAGE-1.1 **As an admin** I'd like to add new books to keep our library up to date.
    * MANAGE-1.1.0 Admin staff needs to be able to manually add new copies to the Library DB using a single form.

    * MANAGE-1.1.1 The form has to be checked for existing data:
        * Authors must not be duplicated
        * Books must not be duplicated
    * MANAGE-1.1.2 Admins should be able to add new copies by providing just the ISBN or the title. The Library will then use the Google Books API to auto-fill the rest of the form.
    * MANAGE-1.1.3 The staff needs to be able to review data fetched from Google Books API before updating the DB.


### Stories for the second sprint

The aim of the second sprint is to develop the admin dashboard for the Library. The following user stories represent requirements for the December release, which was deployed in Jan 2019:


* MANAGE-2.0 **As an admin** I'd like to remove old books *to* keep the library up to date.

    * MANAGE-2.0.0 In 'Admin mode' each copy has a delete button.
    * MANAGE-2.0.1 The delete button removes the entry from the database so that it is no longer displayed.


* FIND-2.0 **As an admin** I'd like to see which books have not been returned *so that* I can order new ones.

    * FIND-2.0.0 The Admin dashboard has to include a list of copies that have been checked out for the longest time.
    * FIND-2.0.1 There should be buttons next to each of these to order new ones.
    * FIND-2.0.2 This needs to be logged as a request ticket.
    * FIND-2.0.3 Ideally clicking the button will automatically place an Amazon order with the right details, or at least add the right book to the basket.


* MANAGE-2.1* **As an admin** I'd like to see users that have not returned books *so* I can chase up the check-in.

    * MANAGE-2.1.0 The Admin dashboard has to include a list of copies that have been checked out for the longest time.
    * MANAGE-2.1.1 There should be a button to send an automated reminder email.


* FIND-2.1 **As a user** I'd like to be able to see who currently has checked out the book *so* I can ask them when they will be done with it.

    * FIND-2.1.0 The name and surname of the person have to be visible next to an unavailable copy.
    * FIND-2.1.1 Potato suggested they would want one page with a list of users and their books - like they do with food.


* FIND-2.2 **As a user** I'd like to see the email address of the person who checked-out out a copy *so that* I can contact them directly.

    * FIND-2.2.0 The email address needs to be displayed next to an unavailable copy.
    * FIND-2.2.1 The user can write an email themselves, but there should be a button to send an automated request email.


* MANAGE-2.2 **As an admin** I'd like to check the status of books *to* know which ones are popular to order more copies.

    * MANAGE-2.2.0 Along with other stats on the admin dashboard, the most popular books/copies are displayed.
    * MANAGE-2.2.1 There should be buttons next to each of these to order new copies.
    * MANAGE-2.2.2 This needs to be logged as a request ticket.
    * MANAGE-2.2.3 Ideally clicking the button will automatically place an Amazon order with the right details, or at least add the right book to the basket.
    * MANAGE-2.2.4 There should be an option to order more copies to the same location or to a different location.


* MANAGE-2.3 **As an admin** I'd like to receive email notifications *so* I know a book has been requested.

    * MANAGE-2.3.0 Everyday the Admin is sent an automated email with all the new request tickets.


* MANAGE-2.4 **As an admin** I'd like to know who requested the book *so* I can notify them.

    * MANAGE-2.4.0 This should be done automatically by the Library system as soon as a ticket is closed.
    * MANAGE-2.4.1 This means that each request ticket must contain information about the user who created the request.


### Stories covered in later sprints

In later sprints we worked on some of the extra requirements as well as updating the app to match additional requests from the client such as aligning the UI theme with that of Potato 4.0. We also made sure the app was more maintainable and easy to work on by redeveloping certain parts like the front end with Vue CLI.

* FIND-3.0 **As a user** I'd like to browse new books available *so* I'm inspired to read.

    * FIND-3.0.0 The web app shall display a list of books with title and author.
    * FIND-3.0.1 The first displayed books should be those most recently added.
    * FIND-3.0.2 New books should be marked with 'NEW' in the app.
    * FIND-3.0.3 After a given time new books are unmarked as new.
    * FIND-3.0.4 Each book should have a thumbnail displayed with it in the books list.


* MANAGE-3.0 **As a user** I'd like to fill in a form with a link to the book to submit to admin.

    * MANAGE-3.0.0 After a failed search result and on the main page, there must be an option to request a book.
    * MANAGE-3.0.1 There shall be a new page with a form which makes up a request that is stored on the database.
    * MANAGE-3.0.2 The admin should then be able to view all the requests through the admin dashboard.
    * MANAGE-3.0.3 Alternatively an email could be sent to the admin with the book.
    * MANAGE-3.0.4 The admin can then check off each of the book requests that have been fulfilled by purchasing a copy.


* MANAGE-3.1 **As a user** I'd like to know when the book I ordered has arrived *so that* I can read it.

    * MANAGE-3.1.0 When the admin adds the book that was requested to the library and removed from the requested book list on the admin dashboard, an email should be sent to all the requesters of that book.
    * MANAGE-3.1.1 If a request is duplicated by two different users then the second email is added to the existing request, and both users are emailed when the request is fulfilled.


* BORROW-3.0 **As a user** I'd like to reserve a requested book *so that* I can read it.

    * BORROW-3.0.0 In the same form for requesting the copy there shall be a checkbox that lets users choose to reserve the book at the same time.
    * BORROW-3.0.1 A table of reservations on the database will be kept up to date for each of the existing and requested copies.


* FIND-3.1**As a user** I'd like to read peer reviews *to* inform my reading.

    * FIND-3.1.0 For any of the books in the database when a user selects it along with the list of copies, a number of reviews will be displayed.
    * FIND-3.1.1 Reviews can be added to any book by users at any point.
    * FIND-3.1.2 Reviews will be stored along with book details in the database.


* FIND-3.2 **As a user** I'd like to read external reviews *to* inform my reading

    * FIND-3.2.0 As well as other user reviews, reviews will be stored and displayed using from the Goodreads or Google books API.
  - Reviews will be shown when a particular book is selected.


* BORROW-3.1 **As a user** I'd like to reserve a book for future dates *to* fit within my schedule.


* BORROW-3.2 **As a user** I'd like to know a books reservations dates *to* know when I can check it out.

    * BORROW-3.2.0 Along with each copy being displayed when you select a book, any reservations will be shown alongside its lending status and location.


* FIND-3.3 **As a user** I'd like to check availability by voice command on Alexa *for* convenience at home.

    * This is not a priority at the moment and is an additional requirement.


* BORROW-3.3 **As a user** I want to ping a coworker *so* they know if I want the book they have.

    * BORROW-3.3.0 If a copy is on loan there will be an option next to the details to send a message to the borrower.
    * BORROW-3.3.1 An email will then be sent to the borrower with information about the request.


* MANAGE-3.2 **As an admin** I'd like to edit all book data *so that* we can improve search.

    * MANAGE-3.2.0 In some 'Admin mode' there will be the possibility to change all fields of the book or copy.
    * MANAGE-3.2.1 Reviews and ratings can be added or deleted.
    * MANAGE-3.2.2 Location can be changed.
    * MANAGE-3.2.3 This directly updates the database.


* BORROW-3.4 **As a user** I'd like to be reminded when I've been holding a book for too long *so that* I can return it for others to read.

    * BORROW-3.4.0 Every day the lendings table of the database will be checked for lends longer than n days.
    * BORROW-3.4.1 Once a lend exceeds that period an automated friendly email is sent to the user.


* FIND-3.4 **As a user** I'd like to filter by multiple locations (e.g. London AND Bristol).

    * FIND-3.4.0 With search bar supply Drop-down of locations.
    * FIND-3.4.1 Have an option to keep adding locations to filter.


* BORROW-3.5 **As a user** I'd like to download an ebook.

    * BORROW-3.5.0 Supply link to where to download ebook with copies.

* FIND-3.5 **As a user** I'd like to see the author *so* I can find the correct book.

    * FIND-3.5.0 In the list of books, the authors of the books will be displayed next to each copy.
    * FIND-3.5.1 The author is stored in an authors table in the database.


* FIND-3.6 **As a user** I'd like to search by author *so that* I can find the right book.

    * FIND-3.6.0 The search feature will be able to be applied to all fields of the book table.
    * FIND-3.6.1 The search query will be processed in the back end and then a new list of books is displayed to the user.


* FIND-3.7 **As a user** I'd like to search by title *so that* I can find the right book.

    * FIND-3.7.0 The search query should be processed in such a way that it returns a list of books with some similarity to the query.

* FIND-3.8 **As a user** I'd like to able to view a book description *so* I know whether I want to read it.

    * FIND-3.8.0 With copies, when a book is selected, there should also be a list of descriptions.
    * FIND-3.8.1 The descriptions should be ordered by relevance or date.

## Non Functional requirements
In addition to functional requirements, we also came up with some non-functional requirements to give us some testable outcomes.

* USABILITY-1.0 Easy to use

    * USABILITY-1.0.0 Having found a book on a shelf, a user can check out the book using the app in less than a minute having never used the app before.

    * USABILITY-1.0.1 User can submit a book request in less than a minute never having used the app before.

* MANAGE-3.3 It is easy for Potato to keep the application running after we have stopped working for them - they are happy with documentation and understand how it all works.

* SAFE-1.0 All user input is validated and the web application is protected from any malicious input to avoid XSS, SQL injection vulnerabilities, etc

* USABILITY-1.1 It isn't slow - all searches should take less than 3 seconds.

* USABILITY-1.2 Clients are happy with the interface (intuitive layout, consistent design).

## Desirable (Non-Essential) features
On top of these requirements, we have also discussed the possibility of adding non-essential, but desirable features. These include Slack integration and Alexa Skills integration. We ultimately decided not to integrate the web app with Alexa Skills because it would be difficult (and slightly awkward) for users to input email addresses via speech through Alexa. Slack integration would have been valuable because Potato employees use it regularly, but we did not have time to implement it.

## Completion of requirements
The functional and non-functional requirements we came up with over the first few weeks provided us with a clear path of how to go about building the application and the features to add at each stage of development. By the end of the project we hadn't fulfilled all of them, but the set that we had completed did constitute a collection that best fit the purpose of the project. As the project progressed, both the clients and our vision of it changed, and to meet new expectations we had to come up with new user stories and features to implement. In the end, the client, Potato were very pleased with the result and felt the final product reflected well their original vision.
