# Library Design Process

The following process describes the steps we took to design each of our 6 releases of the Library App. Before any design process, we considered the whole of backlog together with the Potato team. Using the prioritised User Stories we then updated and/or redesigned any relevant elements.

## *Step 1:* High-level overview of the architecture

This diagram shows the basic interactions between components and the data flows that will exist. This is the final version of the diagram and it is based all 76 completed backlog items. This overview evolved throughout development process as the clients needs changed and we integrated increasingly more elements into our App.

![Architecture diagram](images/application-architecture.png "Architecture Diagram")

## *Step 2:* Library API

Treating the Spring back-end as a black box, we went through the User Stories and updated the RESTful API using Oracle Apiary.
 
Our API evolved throughout the project, below are links to the key versions:
 
- [Sprint 1 - MVP - MINIMUM VIABLE PRODUCT](https://libraryapi6.docs.apiary.io/)
- [Sprint 2 - MLP - MINIMUM LOVABLE PRODUCT](https://librarymlpapi.docs.apiary.io/)
- [Sprint 3 - BETA](https://librarybetaapi.docs.apiary.io/)
- [Sprints 4, 5, 6 - FINAL (Redesign + Feedback Sprints)](https://librarybetabetaapi.docs.apiary.io/) 
 
This API Documentation fully defined the contract between our Spring Backend and Vue.js Frontend. It also implicitly defined the DTOs that Spring had to build. 

Apiary provided our frontend with a mock server with predefined mock data. As a result BE and FE were developed in parallel and there were no issues when we integrated them together. In order to minimise the risks we also merged all our feature branches into the development very often.

## *Step 3:* Crow's foot database diagram

This is the key component of the Library and we agreed that it had to be correctly defined at the beginning of each sprint, as changing relationships in the middle of the sprint could break the entire application. Having updated the API, we now knew the exact DTOs that we would require and therefore could start thinking about new relations between data and how it would be stored. Together we created a crow's foot diagram, which defined the database for the first sprint. 

![Crows foot diagram of DB structure](images/db-schema.png "Database")

The most challenging step was thinking about the relation between Authors and Books (many-to-many) and how to correctly store it. Our final solution involves a middle author_book table. You may also notice that the ID of each Book is its ISBN, however this caused issues with material that doesn't have an ISBN, which Potato still wants in the library. As a retro-action for the second sprint, we redefined the Book so that it has its own auto-incremented ID.

## *Step 4:* UML Diagrams

Finally, we were able to create a static UML diagram of the entities that will be returned by our JPA repositories. There is a direct map between DB Tables and the classes in the diagram. There is only one table missing, "author_book". This table will only be used internally by JPA to generate the set of Authors for Books entities.

![Entities diagram](images/entities-uml.png)

Two methods have been included for the entities. `Author#getFullName` simply combines the forenames and surname of the author into one string. `Copy#isAvailable` returns true if the given copy is currently on a shelf. This uses the set of Lendings that each copy contains. A copy is available if and only if it isn't checked out; i.e. if it does not have any lendings with a `null` check-in timestamp (Lending with a null check-in timestamp means that a copy has been checked out, but never returned).

Using all of the above documentation, we were then able to create UML sequence diagrams for the new features. The diagram below shows the sequences for 2 new features: Pagination for Books followed by Book Request Status Update

![Sequence diagram](images/uml-sequence.png)

## Result

We spent just over a week designing the first version of the Potato Library. Our design strategy gave us a balance of remaining open to future change while eliminating issues such as the potential mismatch between BE and FE. Having designed a solid foundation for our project in October, we only made relatively small changes to it throughout the other sprints. The Potato Team were very happy with the diagrams we provided. They especially praised our BPMN diagram (described in more detail in Requirements). They were also very pleased that we managed to develop the code in parallel for all of the sprints - something that we believe was the result of careful design and planning. 
