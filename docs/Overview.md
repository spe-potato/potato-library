# Potato Books Project Overview

10.10.2018 - 05.04.2019

## The Client 

Our client, [Potato](https://p.ota.to/), is a software development firm with offices located in Bristol, London and San Francisco. They deliver digital products to a range of clients around the world that focus heavily on new and advanced technologies. The company believes in creating a rich and stimulating environment for its employees that allows them to work better. As part of this ethos, they have a large physical collection of books across the offices that they allow their employees to borrow.


## The Problem

Currently, there are over 1000 different copies spread across the Bristol and London offices. Potato has a self-developed library management system in place which only supports the browsing of books and viewing of book details.

When browsing, all of the books are listed in a single web page. This makes finding books difficult due to the long list that users have to go through. Filtering books by certain criteria (particularly location) and search functionality are also not implemented so when searching for books, they currently use the browser's find in page feature. Moreover, there are no ways to easily update the database other than manually editing the actual tables.
 
As a result, it is difficult to actually find books in the respective locations and to keep the inventory up to date. This has resulted in little to no usage of the existing library app.

These are some of the reasons why the clients want a better version of their library management system. These problems have exposed a list of features that will be essential in the final system.

### Current Web Application

##### Browse books

![Browse books](images/old-list.png "Browse books")


##### View book details

![Book details](images/old-page.png "View book details")


## Vision Statement

Potato Books will be an internal web app for Potato employees. It will facilitate a co-operative and self-maintained model for the collection of books across all offices. Unlike the old app, users will be checking books out and easily adding new ones. While this is in contrast to typical, staffed, public libraries, we and the clients believe the employees will keep the inventory up to date themselves once given the right tools to do so.

The aim is that a well functioning app will motivate employees to browse through it and make better use of the Potato book collection, reading in their spare time and furthering their knowledge and personal development - bringing a tangible benefit to the client.

After the completion of our final sprint, we - and the client - believe the vision for the product was absolutely fulfilled, and even exceeded in some places. 

* All users **must** be able to:
    - Search for books
    - Filter books (particularly by location)
    - Check-in borrowed books
    - Check-out books to borrow


* Those performing admin tasks **must** also have the ability to:
    - Add new books
    - Delete books
    - View requested books


This is a summary of the app's essential functionality. See the requirements section for more in-depth explanations and desirable features expressed as user stories and atomic requirements.
