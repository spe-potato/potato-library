# Testing
During all the sprints we chose to embrace Test Driven Development. We have created a total of 138 tests: 26 unit tests and 112 integration tests. They fully describe the intended functionality of the system based on the total of 76 user stories which we were regularly choosing for each sprint. They have been implemented using JUnit4, spring's mockMVC and Testcontainers.

## Final JaCoCo Coverage Report

![Book details](images/final-test-results.png "JaCoCo Report")


As a result of using TDD, the current test coverage is effectively 100%. The overall coverage for the project is precisely 96%, the missing 4% of which is simply caused by unused getters and constructors. Tests are there to make us - the developers - confident in the functionality of our system. Hence, we decided to run all integration tests using a special framework called `Testcontainers`. It provides an easy way to create a fresh MySQL server instance inside a Docker container. Adding Testcontainers to our project was very simple, as it only required a small change to the JDBC URL inside the `application.properties` file. This containerised database uses the exact same version of MySQL that the real Oracle Cloud DB is using. As a result of this, we are confident that the code we develop is and always will be 100% compatible with the deployed instance.

## Writing the tests before any code

One reason why we rely on integration tests so much is that we simply didn't know what the application will look like. Hence we decided to heavily rely on black-box testing. At this stage designing the needed classes first, so that unit tests could be written, was possible. However, it would result in very fragile tests which would be prone to breaking as soon as soon as any refactoring takes place.

At the very beginning of each sprint, we updated our API Documentation and a Crow's Foot diagram. Based on these we were able to create DTOs and write our tests. They don't mock anything and simply execute end-to-end scenarios by hitting our REST endpoints, i.e. the API commands we defined. 

The downside of this approach is that bugs can be hard to trace. Conversely, it did allow us to write all tests without assuming anything about the internal structure of our backend (other than the fact that it implements the API and has access to the fake DB). As a result of this approach, we are able to carry out large refactorings of our codebase and still receive instant feedback on the changes we've made. Even the creation of new classes did not break any tests, which would've been the case if we used a mocking framework and more focused unit tests.


Here is what an example of one of our mockMvc tests looks like:

```Java

    @Test
    public void doubleBorrowCopy31Test() throws Exception {

        ResultActions a = this.mvc.perform(put("/copies/31/check_in"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy31Returned()));

        ResultActions b = this.mvc.perform(put("/copies/31/check_out"))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy31()));

        ResultActions c = this.mvc.perform(put("/copies/31/check_out"))
                .andExpect(status().isBadRequest());
    }
```

In the above example `Mock.Copy31` is a pre-defined CopyDTO, the structure of which comes from the API docs. The `expect` method converts the DTO to JSON and then finally `checkJson` compares the expected JSON string with the one that was returned from the API call.

All of our controllers and their methods were tested in the same way. We also made sure that edge cases are covered by our tests; some of our tests try to get books that don't exist, borrow copies that are already checked out (as seen above) or for example get books that contain polish characters in their title or description. In fact, the last case helped us to discover an issue with UTF-8 being incorrectly configured.

As part of the first sprint, we had to develop lending logic, which was somewhat complicated. For that particular component we decided to create 8 unit tests. Once we had the controllers ready and fully functional, we also created 12 unit tests to make sure that the parameters that get passed in with the request match the API requirements. In particular, we needed to make sure that the email address supplied was correct because in a later sprint we used that address to communicate with the users.

There are some issues with using a full MySQL database for the majority of our tests. The tests take between 15 to 20 seconds to run, which is a lot compared to the nearly instant feedback of standard unit tests. In order to reduce the time needed for feedback, we started using Maven's Failsafe and Surefire plugins together. The unit tests are the first to run using Surefire and the integration tests are only executed if the unit tests have passed. There also existed a possibility of one failed test mutating the database in a way that would affect other test(s) and cause a domino of failures despite only one "real" failure. We looked into using DbUnit and its `@ExpectedDatabase` annotation to avoid such issues - however in the end, we found an even easier solution using Spring's own `@Transactional` annotation which reverses any actions done on the database after each test. To further improve the quality of our tests we planned to write a few more to make sure that the system fully supports fields containing emoji - something that OpenProject erroring upon emoji-containing input raised to us. Their presence is unlikely in book titles or descriptions, but it's likely that they will be present in the user's reviews.

## Static Code Analysis
### Finding bugs that have not happened yet

Testing is extremely important as it can verify the correctness of our implementation against the API we defined. However, it cannot ensure code quality and maintainability. A test-passing implementation is often far from perfect. To cope with this, we decided to include Codacy and Sonarcloud static code analysis tools as part of our Gitlab CI pipeline. It resulted in a significant reduction in code smells and bugs and most importantly it identified duplicate code in our controllers, which we subsequently eliminated. Now the amount of duplicate code is precisely 0 lines.

![Static code analysis details](images/sonarcloud.png "Sonarcloud Report")

![Static code analysis example results](images/example-smells.png "Example Code Smells")
