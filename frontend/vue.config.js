// const PurgecssPlugin = require("purgecss-webpack-plugin");
// const glob = require("glob-all");
// const path = require("path");

module.exports = {
    configureWebpack: {
        resolve: {
            alias: require("./aliases.config").webpack
        }
    },
    chainWebpack: config => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
                options['transformAssetUrls'] = {
                    img: 'src',
                    image: 'xlink:href',
                    'b-img': 'src',
                    'b-img-lazy': ['src', 'blank-src'],
                    'b-card': 'img-src',
                    'b-card-img': 'img-src',
                    'b-carousel-slide': 'img-src',
                    'b-embed': 'src'
                }

                return options
            })
    },
    css: {
        sourceMap: true
    },
    devServer: {
        proxy: {
            "/api": {
                target: "http://localhost:8088",
                ws: true,
                changeOrigin: true
            }
        }
    },
    outputDir: "target/dist",
    assetsDir: "static"
};
