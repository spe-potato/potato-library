const path = require("path");

function resolveSrc(_path) {
    return path.join(__dirname, _path);
}

const aliases = {
    "@": "src",
    "@src": "src",
    "bootstrap-components": "node_modules/bootstrap-vue/es/components",
    "bootstrap-directives": "node_modules/bootstrap-vue/es/directives"
};

module.exports = {
    webpack: {},
    jest: {}
};

for (const alias in aliases) {
    module.exports.webpack[alias] = resolveSrc(aliases[alias]);
    module.exports.jest["^" + alias + "/(.*)$"] =
        "<rootDir>/" + aliases[alias] + "/$1";
}
