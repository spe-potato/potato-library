// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";

// import vuelidate
import Vuelidate from "vuelidate";
Vue.use(Vuelidate);

// import vue-reactive-cookie
import VueReactiveCookie from "vue-reactive-cookie";
Vue.use(VueReactiveCookie);

// import vue-anime.js
import VueAnime from 'vue-animejs';
Vue.use(VueAnime);

// import vue-async-computed
import AsyncComputed from 'vue-async-computed'
Vue.use(AsyncComputed)

// import vue-debounce
import vueDebounce from 'vue-debounce'
Vue.use(vueDebounce, {
    listenTo: 'oninput' // only respond to input
})

import VueScrollTo from 'vue-scrollto'
Vue.use(VueScrollTo, {
    container: "body",
    duration: 500,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
})


// import fontawesome-vue (icons)
import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faCheck,
    faChevronUp,
    faExclamationCircle,
    faCog,
    faSignOutAlt,
    faBarcode,
    faPlus,
    faMinus,
    faUser,
    faLink
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(
    faCheck,
    faChevronUp,
    faExclamationCircle,
    faCog,
    faSignOutAlt,
    faBarcode,
    faPlus,
    faMinus,
    faUser,
    faLink,
    faCamera
);
Vue.component("font-awesome-icon", FontAwesomeIcon);

// import main app component
import App from "./App";

// import the router config
import router from "./router";

// import stylesheet
import "@/style/stylesheet.scss";
import {faCamera} from "@fortawesome/free-solid-svg-icons/faCamera";

// turns off a "tip" that shows in console with Vue in non-production mode
Vue.config.productionTip = false;



/* eslint-disable no-new */
new Vue({
    router,
    render: h => h(App)
}).$mount("#app");
