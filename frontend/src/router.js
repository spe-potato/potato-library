import Vue from "vue";
import Router from "vue-router";

const BooksView = () => import(/* webpackChunkName: "books-view" */ "@/views/BooksView");
const SingleBookView = () => import(/* webpackChunkName: "single-book-view" */ "@/views/SingleBookView");
const AdminView = () => import(/* webpackChunkName: "admin" */ "@/views/admin/AdminView");
const OverdueCopies = () => import(/* webpackChunkName: "admin" */ "@/views/admin/OverdueCopies");
const Requests = () => import(/* webpackChunkName: "admin" */ "@/views/admin/Requests");
const AddBook = () => import(/* webpackChunkName: "admin" */ "@/views/admin/AddBook")
const AddCopy = () => import(/* webpackChunkName: "admin" */ "@/views/admin/AddCopy")
const DeleteBookCopy = () => import(/* webpackChunkName: "add-copy" */ "@/views/admin/DeleteBookCopy");
const MyBooks = () => import(/* webpackChunkName: "my-books" */ "@/views/MyBooks");
const RequestBook = () => import(/* webpackChunkName: "request-book" */ "@/views/RequestBook");

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            redirect: { name: "booksView" }
        },
        {
            path: "/books",
            name: "booksView",
            component: BooksView
        },
        {
            path: "/book/:id",
            name: "singleBookView",
            component: SingleBookView,
            props(route) {
                const props = { ...route.params };
                props.id = parseInt(props.id);
                return props;
            }
        },
        {
            path: "/admin",
            name: "adminView",
            component: AdminView,
            redirect: { name: "overdueCopies" },
            children: [
                {
                    path: "overdue-copies",
                    name: "overdueCopies",
                    component: OverdueCopies
                },
                {
                    path: "requests",
                    name: "requests",
                    component: Requests
                },
                {
                    path: "add-book",
                    name: "addBook",
                    component: AddBook
                },
                {
                    path: "add-copy/:id?",
                    name: "addCopy",
                    component: AddCopy
                },
                {
                    path: "delete/:id?",
                    name: "deleteBookCopy",
                    component: DeleteBookCopy
                }
            ]
        },
        {
            path: "/my-books",
            name: "myBooks",
            component: MyBooks
        },
        {
            path: "/request-book",
            name: "requestBook",
            component: RequestBook
        }
    ]
});
