FROM openjdk:8-jre

# Specify these environment variables at runtime with the '-e' flag
#ENV LIBRARY_DB_HOSTNAME=
#ENV LIBRARY_DB_USERNAME=
#ENV LIBRARY_DB_PASSWORD=

ENV PORT=80

ARG JAR_FILE
COPY app/target/${JAR_FILE} /usr/share/potato/potatolibrary.jar

ENTRYPOINT ["java", "-jar", "/usr/share/potato/potatolibrary.jar"]

#docker build . --build-arg JAR_FILE=potato-library-3.0.0-beta.jar -t potatospe/potato-library:3.0.0-beta
#docker push potatospe/potato-library:3.0.0-beta
#docker run -p 8080:8080 -e LIBRARY_DB_HOSTNAME=$LIBRARY_DB_HOSTNAME -e LIBRARY_DB_USERNAME=$LIBRARY_DB_USERNAME -e LIBRARY_DB_PASSWORD=$LIBRARY_DB_PASSWORD -t potatospe/potato-library:3.0.0-beta
