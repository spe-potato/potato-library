USE `library`;

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE TABLE author;
TRUNCATE TABLE author_book;
TRUNCATE TABLE book;
TRUNCATE TABLE copy;
TRUNCATE TABLE lending;
TRUNCATE TABLE request;
TRUNCATE TABLE review;

SET FOREIGN_KEY_CHECKS = 1;