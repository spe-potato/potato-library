package uk.ac.cs.bristol.potatolibrary.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.checkJson;
import static uk.ac.cs.bristol.potatolibrary.Tester.expect;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockLendingTests {

    @Autowired
    private MockMvc mvc;

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void simpleBorrowTest() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/copies/2/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy2Popularity1()).notAvailable());

        ResultActions b = this.mvc.perform(put("/api/copies/2/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy2Popularity1()).available());

    }

    @Test
    public void borrowIncorrectEmailTest() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/copies/2/check_out")
                .param("email", "test@potato"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void simpleReturnTest() throws Exception {

        ResultActions a = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy31()).available());
    }

    @Test
    public void returnWithBadEmailTest() throws Exception {

        ResultActions a = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test2@example.com"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void doubleBorrowCopy2Test() throws Exception {

        ResultActions a = this.mvc.perform(put("/api/copies/2/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy2Popularity1()).notAvailable());

        ResultActions b = this.mvc.perform(put("/api/copies/2/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isBadRequest());
    }

    @Test // expect bad request if double return
    public void doubleReturnCopy31Test() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy31()).available());


        ResultActions b = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isBadRequest());
    }


    @Test // expect bad request if double return
    public void doubleBorrowCopy31Test() throws Exception {

        ResultActions a = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy31()).available());

        ResultActions b = this.mvc.perform(put("/api/copies/31/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy31Popularity2()).notAvailable());

        ResultActions c = this.mvc.perform(put("/api/copies/31/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isBadRequest());
    }

    @Test // somehow actually check the borrow was successful and the database was updated
    public void dataActuallyUpdatesTest() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy31()).available());

        ResultActions b = this.mvc.perform(get("/api/copies/31")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy31()).available());

        ResultActions x = this.mvc.perform(put("/api/copies/31/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        x = checkJson(x, expect(Mock.Copy31Popularity2()).notAvailable());

        ResultActions r = this.mvc.perform(get("/api/copies/31")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy31Popularity2()).notAvailable());

    }

    @Test
    public void checkInNonExistentCopyTest() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/copies/999/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isNotFound());
    }

}
