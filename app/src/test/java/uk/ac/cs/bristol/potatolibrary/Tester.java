package uk.ac.cs.bristol.potatolibrary;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.google.gson.*;
import org.hamcrest.Matchers;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.CopyDTO;
import uk.ac.cs.bristol.potatolibrary.testdata.ExpectedJsonObject;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * Class responsible for all assertions relating to JSON content returned by the API calls.
 * <p>
 * The general flow for each test:
 * <p>
 * 1. this.mvc.perform([SOME API REQUEST])  -> Perform an API request
 * 2. .andExpect(status().isOk());          -> Expect OK (HTTP code 200)
 * 3. #checkJson                            -> Compare the returned JSON with expected JSON
 * <p>
 * Look below for details on how #checkJson works
 */
public class Tester {


    public static ExpectedJsonObject expect(Object o) throws JsonProcessingException {
        return new ExpectedJsonObject(Tester.getJackson().writeValueAsString(o));
    }

    //    TYPICAL ERROR:
    //
    //    java.lang.AssertionError: authors[first_name=Gino]
    //    Expected: a JSON object
    //    but none found
    //    ; authors[first_name=Dan]
    //    Unexpected: a JSON object
    //
    //    MEANING: Expected "Gino" but found "Dan"

    /**
     * Compares the actual JSON contained in the {@code ResultActions} with expected JSON.
     * The assertions are not strict, meaning that the actual JSON can contain more information than expexted.
     * As long as the required information is there, the tests will pass.
     * <p>
     * This methods works by comparing Strings.
     * The required information might be nested under some other JSON objects or arrays, so it has to be retrieved.
     * This is done by the {@link #getActualJsonString(ResultActions, Optional, boolean)} method.
     *
     * @param r        The object returned by mvc#perform
     * @param expected A wrapper object containing the expected JSON
     *
     * @return The same ResultActions (not sure if neccessary!)
     *
     * @throws Exception if there are any assertion failures
     */
    public static ResultActions checkJson(ResultActions r, ExpectedJsonObject expected) throws Exception {


        String actualJsonString = getActualJsonString(r, expected.getIndex(), expected.isInsidePageContent());

//        System.out.println("ACTUAL JSON:   " + new JSONObject(actualJsonString).toString(4));
//        System.out.println("EXPECTED JSON: " + new JSONObject(expected.getJsonString()).toString(4));

        System.out.println("ACTUAL JSON:   " + actualJsonString);
        System.out.println("EXPECTED JSON: " + expected.getJsonString());


        JSONAssert.assertEquals(expected.getJsonString(), actualJsonString, expected.isStrict());

        if (expected.getAvailable().isPresent()) {
            if (expected.getAvailable().get()) {
                CopyDTO c = getGson().fromJson(actualJsonString, CopyDTO.class);
                assertNotNull(c.getLending().getCheckInDate());
            } else {
                CopyDTO c = getGson().fromJson(actualJsonString, CopyDTO.class);
                assertNull(c.getLending().getCheckInDate());
            }
        }

        return r;
    }

    /**
     * Creates a Result Matcher which will check the content for its contentSize.
     * This matcher should then be passed as an argument to the {@code #andExpect} method.
     * <p>
     * Example: {@code this.mvc.perform([SOME API CALL]).andExpect(contentSize(2)}
     *
     * @param size The expected contentSize of content
     *
     * @return a ResultMatcher which will look at contentSize of the content
     */
    @NotNull
    public static ResultMatcher contentSize(int size) {
        return jsonPath("$.content", Matchers.hasSize(size));
    }

    public static ResultMatcher size(int size) {
        return jsonPath("$", Matchers.hasSize(size));
    }

    /**
     * There are 4 possible types of strings we might want to fetch:
     * <p>
     * 1. If JSON is a Page object: String from Page.content[index]
     * 2. If JSON is a Page object: String from Page.content
     * 3. If JSON is an array of objects: String at some position in that array
     * 4. In all cases: String representing the entire JSON resposnse
     * <p>
     * Which one we want, depends on what we want to test.
     * What we want to test is specified by {@code index} and {@code insidePage}
     * <p>
     * Inside Page & Index is present: Case 1
     * Inside Page & Index not present: Case 2
     * No Page but Index is present: Case 3
     * No Page or Index: Case 4
     *
     * @param r          The object returned by mvc#perform
     * @param index      Optional index value if an object inside an array is to be fetched
     * @param insidePage If true, the methods will only look at JSON under Page.Content
     *
     * @return retrieved JSON string
     *
     * @throws UnsupportedEncodingException if there are any issues with JSON being correctly parsed
     */
    @NotNull
    private static String getActualJsonString(ResultActions r, Optional<Integer> index, boolean insidePage) throws UnsupportedEncodingException {
        String jsonString;

        if (insidePage) {
            if (index.isPresent()) {
                jsonString = getJsonStringFromArrayInsidePageContent(r, index.get());
            } else {
                jsonString = getJsonStringFromJustInsidePageContent(r);
            }
        } else {
            if (index.isPresent()) {
                jsonString = getJsonStringFromArray(r, index.get());
            } else {
                jsonString = getPlainJsonString(r);
            }
        }

        return jsonString;
    }

    @NotNull
    private static String getJsonStringFromJustInsidePageContent(ResultActions r) throws UnsupportedEncodingException {
        return new JSONObject(getPlainJsonString(r)).getJSONArray("content").toString();
    }


    @NotNull
    private static String getJsonStringFromArrayInsidePageContent(ResultActions r, int index) throws UnsupportedEncodingException {
        return new JSONObject(getPlainJsonString(r)).getJSONArray("content").get(index).toString();
    }

    private static String getJsonStringFromArray(ResultActions r, int index) throws UnsupportedEncodingException {
        return new JSONArray(getPlainJsonString(r)).get(index).toString();
    }

    @NotNull
    private static String getPlainJsonString(ResultActions r) throws UnsupportedEncodingException {
        MvcResult mvcResult = r.andReturn();
        return mvcResult.getResponse().getContentAsString();
    }

    public static Gson getGson() {
        return new GsonBuilder()
//                .setPrettyPrinting()
                .registerTypeAdapter(
                        LocalDateTime.class, (JsonDeserializer<LocalDateTime>) (json, type, jsonDeserializationContext) ->
                                LocalDateTime.parse(json.getAsJsonPrimitive().getAsString()))
                .registerTypeAdapter(LocalDateTime.class, (JsonSerializer<LocalDateTime>) (src, typeOfSrc, context) -> {
                    String date = src.toString();
                    return new JsonPrimitive(date);
                })
                .create();
    }

    public static ObjectMapper getJackson() {
        return new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule())
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    private class Page {
        List<Object> content;
    }
}
