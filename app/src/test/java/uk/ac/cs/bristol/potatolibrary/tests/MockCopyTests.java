package uk.ac.cs.bristol.potatolibrary.tests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.CopyDTO;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockCopyTests {

    @Autowired
    private MockMvc mvc;

    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();


    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void getCopy2ByCopyID() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/2"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy2()));
    }

    @Test
    public void getCopy130ByCopyID() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/130"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy130()));
    }

    @Test // Also checks if the book inside the copy is correct
    public void getCopy31ByCopyID() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/31"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy31()));
    }

    @Test
    public void getCopyByIdNotFound() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/999"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getCopiesByBookIdOfBook1() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/?bookid=1"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy2()).atPosition(0));
    }

    @Test
    public void getCopiesByBookIdOfBook2() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/?bookid=2"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
    }


    @Test
    public void getCopiesByBookIdOfBook20() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies/?bookid=20"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(ImmutableList.of(
                Mock.Copy22(),
                Mock.Copy130(),
                Mock.Copy2567()
        )));

    }

    @Test
    public void addCopyTest() throws Exception {
        ResultActions a = this.mvc.perform(
                post("/api/copies")
                        .content(gson.toJson(Mock.Copy24()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        a = checkJson(a, expect(Mock.Copy24Created()).atPosition(0));

        JSONArray j = new JSONArray(a.andReturn().getResponse().getContentAsString());

        CopyDTO createdCopy = gson.fromJson(j.get(0).toString(), CopyDTO.class);

        ResultActions b = this.mvc.perform(get("/api/copies/" + createdCopy.getId()))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy24Created()));

        ResultActions c = this.mvc.perform(delete("/api/copies/" + createdCopy.getId()))
                .andExpect(status().isNoContent());

        ResultActions d = this.mvc.perform(get("/api/copies/" + createdCopy.getId()))
                .andExpect(status().isNotFound());

    }

    @Test
    public void addBadCopyTest() throws Exception {
        ResultActions a = this.mvc.perform(
                post("/api/copies")
                        .content(gson.toJson(Mock.BadCopy24()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void addManyCopiesTest() throws Exception {
        ResultActions a = this.mvc.perform(
                post("/api/copies")
                        .content(gson.toJson(Mock.Copy24()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .param("quantity", "123"))
                .andExpect(status().isCreated());

        JSONArray j = new JSONArray(a.andReturn().getResponse().getContentAsString());

        assertEquals(123, j.length());

        CopyDTO createdCopy1 = gson.fromJson(j.get(5).toString(), CopyDTO.class);
        CopyDTO createdCopy2 = gson.fromJson(j.get(50).toString(), CopyDTO.class);

        assertNotEquals(createdCopy1.getId(), createdCopy2.getId());

        createdCopy1 = gson.fromJson(j.get(45).toString(), CopyDTO.class);
        createdCopy2 = gson.fromJson(j.get(100).toString(), CopyDTO.class);

        assertNotEquals(createdCopy1.getId(), createdCopy2.getId());

        createdCopy1 = gson.fromJson(j.get(7).toString(), CopyDTO.class);
        createdCopy2 = gson.fromJson(j.get(56).toString(), CopyDTO.class);

        assertNotEquals(createdCopy1.getId(), createdCopy2.getId());

        ResultActions b = this.mvc.perform(get("/api/copies/")
                .param("bookid", "13")
                .param("limit", "200"))
                .andExpect(status().isOk())
                .andExpect(size(124));

        b = checkJson(b, expect(Mock.Copy24Created()).atPosition(50));
        JSONArray array = new JSONArray(b.andReturn().getResponse().getContentAsString());

        CopyDTO createdCopy = gson.fromJson(array.get(12).toString(), CopyDTO.class);

        ResultActions c = this.mvc.perform(delete("/api/copies/" + createdCopy.getId()))
                .andExpect(status().isNoContent());

        ResultActions d = this.mvc.perform(get("/api/copies/" + createdCopy.getId()))
                .andExpect(status().isNotFound());

    }

    @Test
    public void removeNonExistentCopyTest() throws Exception {
        ResultActions a = this.mvc.perform(delete("/api/copies/4678898"))
                .andExpect(status().isNotFound());
    }

}

// TODO: 01/03/19 Test chain deletion (delete books should cause copies, requests, reviews to also be removed!)