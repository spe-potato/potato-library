package uk.ac.cs.bristol.potatolibrary.testdata;

import lombok.Getter;

import java.util.Optional;

/**
 * Small wrapper object which stores the expected JSON String.
 * It also stores information about where this JSON can be found in the actual JSON.
 */
@Getter
public class ExpectedJsonObject {

    // The expected JSON String
    private String jsonString;

    // Optional information about the string
    private Optional<Integer> index;
    private Optional<Boolean> available;
    private boolean insidePageContent;
    private boolean strict;

    public ExpectedJsonObject(String jsonString) {
        this.jsonString = jsonString;
        this.index = Optional.empty();
        this.available = Optional.empty();
        this.insidePageContent = false;
        this.strict = false;
    }

    /**
     * Specifies that the JSON String should be present at some position in an array.
     *
     * @param index Position of the array to look at.
     *
     * @return ExpectedJsonObject so that methods can be chained (like a builder)
     */
    public ExpectedJsonObject atPosition(int index) {
        this.index = Optional.of(index);
        return this;
    }

    /**
     * Specifies that the JSON String should be present inside Page.content.
     *
     * @return ExpectedJsonObject so that methods can be chained (like a builder)
     */
    public ExpectedJsonObject insidePageContent() {
        this.insidePageContent = true;
        return this;
    }

    public ExpectedJsonObject available() {
        this.available = Optional.of(true);
        return this;
    }

    public ExpectedJsonObject notAvailable() {
        this.available = Optional.of(false);
        return this;
    }
}
