package uk.ac.cs.bristol.potatolibrary.tests;

import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import uk.ac.cs.bristol.potatolibrary.Tester;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.ReviewDTO;
import uk.ac.cs.bristol.potatolibrary.testdata.TestCases;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.checkJson;
import static uk.ac.cs.bristol.potatolibrary.Tester.expect;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockReviewTests {

    @Autowired
    private MockMvc mvc;

    private final Gson gson = Tester.getGson();

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void getReview2ByReviewId() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/2"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(TestCases.Mock.Review2()));
    }

    @Test
    public void getReviews() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review2()).insidePageContent().atPosition(0));
        r = checkJson(r, expect(Mock.Review3()).insidePageContent().atPosition(1));
        r = checkJson(r, expect(Mock.Review1()).insidePageContent().atPosition(2));
    }

    @Test
    public void getReviewsByBookId() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/")
                .param("bookid", "15"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review3()).insidePageContent().atPosition(0));
        r = checkJson(r, expect(Mock.Review1()).insidePageContent().atPosition(1));
    }

    @Test
    public void addReview() throws Exception {

        LocalDateTime beforeApiCall = LocalDateTime.now();

        ResultActions r = this.mvc.perform(post("/api/reviews/")
                .content(gson.toJson(TestCases.Mock.Review4()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        r = checkJson(r, expect(TestCases.Mock.Review4()));

        ReviewDTO createdReview = gson.fromJson(r.andReturn().getResponse().getContentAsString(), ReviewDTO.class);

        ResultActions b = this.mvc.perform(get("/api/reviews/" + createdReview.getId()))
                .andExpect(status().isOk());
        b = checkJson(b, expect(TestCases.Mock.Review4Created()));

        assertNotNull("Review ID was null", createdReview.getId());
        assertNotNull("Date Created was null", createdReview.getDateCreated());
        assertTrue("Date created was too early", createdReview.getDateCreated().compareTo(beforeApiCall) > 0);

        ResultActions c = this.mvc.perform(delete("/api/reviews/" + createdReview.getId()))
                .andExpect(status().isNoContent());

        ResultActions d = this.mvc.perform(get("/api/reviews/" + createdReview.getId()))
                .andExpect(status().isNotFound());

    }

    @Test
    public void getReviewsByAuthorEmail() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/")
                .param("email", "potatolibrary2@gmail.com"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review2()).insidePageContent().atPosition(0));
    }

    @Test
    public void getReviewByIdNotFound() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/999"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void removeNonExistentReview() throws Exception {
        ResultActions a = this.mvc.perform(delete("/api/reviews/4678898"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getReviewsSortedByRating() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/")
                .param("sortby", "rating"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review1()).insidePageContent().atPosition(0));
        r = checkJson(r, expect(Mock.Review3()).insidePageContent().atPosition(1));
        r = checkJson(r, expect(Mock.Review2()).insidePageContent().atPosition(2));

    }

    @Test
    public void getReviewsSortedByRatingDESC() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/")
                .param("sortby", "rating")
                .param("sortdirection", "DESC"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review1()).insidePageContent().atPosition(0));
        r = checkJson(r, expect(Mock.Review3()).insidePageContent().atPosition(1));
        r = checkJson(r, expect(Mock.Review2()).insidePageContent().atPosition(2));

    }

    @Test
    public void getReviewsSortedByDate() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/")
                .param("sortby", "date"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review2()).insidePageContent().atPosition(0));
        r = checkJson(r, expect(Mock.Review3()).insidePageContent().atPosition(1));
        r = checkJson(r, expect(Mock.Review1()).insidePageContent().atPosition(2));
    }

    @Test
    public void getReviewsSortedByDateDESC() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/reviews/")
                .param("sortby", "date"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Review2()).insidePageContent().atPosition(0));
        r = checkJson(r, expect(Mock.Review3()).insidePageContent().atPosition(1));
        r = checkJson(r, expect(Mock.Review1()).insidePageContent().atPosition(2));
    }

    @Test
    public void getAverageRating() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/books/15/"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Book15WithAverageRating()));
    }

}
