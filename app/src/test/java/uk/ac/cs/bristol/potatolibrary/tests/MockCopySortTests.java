package uk.ac.cs.bristol.potatolibrary.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockCopySortTests {

    @Autowired
    private MockMvc mvc;

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void getSfCopiesSortedById() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("location", "sf")
                .param("sortby", "copy_id"))
                .andExpect(status().isOk())
                .andExpect(size(2));
        r = checkJson(r, expect(Mock.Copy22()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy31()).atPosition(1));
    }

    @Test
    public void getAllSortedByCheckOutDate() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("sortby", "checkout_date"))
                .andExpect(status().isOk())
                .andExpect(size(25));
        r = checkJson(r, expect(Mock.Copy5()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(1));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(2));
        r = checkJson(r, expect(Mock.Copy31()).atPosition(3));
    }

    @Test
    public void getAllSortedByCheckOutDateWithAvailabilityAll() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("sortby", "checkout_date")
                .param("availability", "all"))
                .andExpect(status().isOk())
                .andExpect(size(25));
        r = checkJson(r, expect(Mock.Copy5()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(1));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(2));
        r = checkJson(r, expect(Mock.Copy31()).atPosition(3));
    }

    @Test
    public void getOnlyAvailableByBookId() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("bookid", "2")
                .param("sortby", "copy_id")
                .param("availability", "available"))
                .andExpect(status().isOk())
                .andExpect(size(1));
        r = checkJson(r, expect(Mock.Copy4()).atPosition(0));
    }

    @Test
    public void getUnavailableSortedByCheckOutDate() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("sortby", "checkout_date")
                .param("availability", "unavailable"))
                .andExpect(status().isOk())
                .andExpect(size(3));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(1));
        r = checkJson(r, expect(Mock.Copy31()).atPosition(2));
    }

    @Test
    public void getUnavailableSortedByCheckOutDateWithLocation() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("sortby", "checkout_date")
                .param("availability", "unavailable")
                .param("location", "london"))
                .andExpect(status().isOk())
                .andExpect(size(2));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(1));
    }

    @Test
    public void getAllCurrentlyCheckedOutByUser() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("availability", "unavailable")
                .param("sortby", "copy_id"))
                .andExpect(status().isOk())
                .andExpect(size(2));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(1));
    }

    @Test
    public void getAllEverCheckedOutByUser() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("availability", "all")
                .param("sortby", "copy_id"))
                .andExpect(status().isOk())
                .andExpect(size(3));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy5()).atPosition(1));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(2));
    }

    @Test
    public void getAllEverCheckedOutByUserSortedByCheckOutDate() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("availability", "all")
                .param("sortby", "checkout_date"))
                .andExpect(status().isOk())
                .andExpect(size(3));
        r = checkJson(r, expect(Mock.Copy5()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(1));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(2));
    }

    @Test
    public void getAllCurrentlyCheckedOutByUserSortedByMostOverdue() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("availability", "unavailable")
                .param("sortby", "checkout_date"))
                .andExpect(status().isOk())
                .andExpect(size(2));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(1));
    }

    @Test
    public void getAllCurrentlyCheckedOutByUserInLondonSortedByMostOverdue() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("availability", "unavailable")
                .param("sortby", "checkout_date")
                .param("location", "london"))
                .andExpect(status().isOk())
                .andExpect(size(2));
        r = checkJson(r, expect(Mock.Copy3()).atPosition(0));
        r = checkJson(r, expect(Mock.Copy8()).atPosition(1));
    }

    @Test
    public void getAllEverCheckedOutByUserInBristolSortedByMostOverdue() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("sortby", "checkout_date")
                .param("location", "bristol"))
                .andExpect(status().isOk())
                .andExpect(size(1));
        r = checkJson(r, expect(Mock.Copy5()).atPosition(0));
    }


    @Test
    public void getAllCurrentlyCheckedOutByUserInSf() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("email", "kg17815@my.bristol.ac.uk")
                .param("sortby", "checkout_date")
                .param("location", "sf"))
                .andExpect(status().isOk())
                .andExpect(size(0));
    }

    @Test
    public void duplicateCopiesBugTest() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/copies/5/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy5Popularity2()).notAvailable());

        ResultActions b = this.mvc.perform(put("/api/copies/5/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy5Popularity2()).available());

        ResultActions c = this.mvc.perform(put("/api/copies/5/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        c = checkJson(c, expect(Mock.Copy5Popularity3()).notAvailable());

        ResultActions d = this.mvc.perform(put("/api/copies/5/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        d = checkJson(d, expect(Mock.Copy5Popularity3()).available());

        ResultActions e = this.mvc.perform(put("/api/copies/5/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        e = checkJson(e, expect(Mock.Copy5Popularity4()).notAvailable());

        ResultActions r = this.mvc.perform(get("/api/copies")
                .param("bookid", "3")
                .param("sortby", "checkout_date")
                .param("sortDirection", "ASC"))
                .andExpect(status().isOk())
                .andExpect(size(1));

        r = checkJson(r, expect(Mock.Copy5Popularity4()).atPosition(0));
    }

    // TODO: 22/02/19 Test popularity sort direction

    // FIXME: 31/01/19 "copies endpoint if you try sort by checkout_date and filter by email checked out by it returns internal server error"

    /*
        the three params that gave an error were

        sorby:          checkout_date,
        availability:   unavailable,
        email:          tombarrie@potatolondon.com

        but if you remove the sortby it works fine
     */

}
