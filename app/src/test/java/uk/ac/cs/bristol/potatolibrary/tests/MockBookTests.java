package uk.ac.cs.bristol.potatolibrary.tests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;

import javax.transaction.Transactional;
import java.util.Map;

import static org.assertj.core.api.BDDAssertions.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;


/**
 * Tests all the API methods provided under /books
 * These are all end-to-end integration tests as explained in docs/Testing.md
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockBookTests {

    @Value("${local.management.port}")
    private int mgt;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private MockMvc mvc;


    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void oneBookTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books?limit=1"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));
        a = checkJson(a, expect(Mock.Book1()).insidePageContent().atPosition(0));
    }

    @Test
    public void firstBookOnFirstPageTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));
        a = checkJson(a, expect(Mock.Book1()).insidePageContent().atPosition(0));
    }

    @Test
    public void firstTwoBooksOnFirstPageTestA() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book1()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(1));
    }

    @Test
    public void firstTwoBooksOnFirstPageTestB() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books?page=1"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book1()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(1));
    }

    @Test
    public void firstTwoBooksOnFirstPageTestC() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books?page=1&limit=3"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book1()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(1));
    }

    @Test
    public void lastBookOnFirstPageTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book20()).insidePageContent().atPosition(19));
    }

    @Test
    public void firstBookOnSecondPageTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books?page=2"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book21()).insidePageContent().atPosition(0));
    }

    @Test
    public void specialCharactersBookTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book11()).insidePageContent().atPosition(10));
    }

    @Test
    public void polishCharactersBookTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book12()).insidePageContent().atPosition(11));
    }

    @Test
    public void getBook10ById() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books/10"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book10()));
    }

    @Test
    public void getBook11ById() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books/11"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book11()));
    }

    @Test
    public void getBook12ById() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books/12"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book12()));
    }

    @Test
    public void getBookByIsbnNotFound() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books/999"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void firstPageSfLocationFilter() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books?location=sf"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(contentSize(2));

        a = checkJson(a, expect(ImmutableList.of(Mock.Book12(), Mock.Book20())).insidePageContent());
    }

    @Test
    public void locationFilterAndSearch() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books?location=bristol&query=Phasellus"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(contentSize(1));

        a = checkJson(a, expect(Mock.Book3()).atPosition(0).insidePageContent());
    }

    @Test
    public void addAndRemoveBook() throws Exception {

        ResultActions b = this.mvc.perform(post("/api/books")
                .content(gson.toJson(Mock.Book22()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
        b = checkJson(b, expect(Mock.Book22()));

        BookDTO createdBook = gson.fromJson(b.andReturn().getResponse().getContentAsString(), BookDTO.class);

        ResultActions c = this.mvc.perform(get("/api/books/" + createdBook.getId()))
                .andExpect(status().isOk());
        c = checkJson(c, expect(Mock.Book22()));

        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "potato person"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author707()).insidePageContent().atPosition(0));

        ResultActions d = this.mvc.perform(delete("/api/books/" + createdBook.getId()))
                .andExpect(status().isNoContent());

        ResultActions e = this.mvc.perform(get("/api/books/" + createdBook.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addAndRemoveBookWhenOneAuthorExistsAndOneDoesnt() throws Exception {

        ResultActions b = this.mvc.perform(post("/api/books")
                .content(gson.toJson(Mock.Book25()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
        b = checkJson(b, expect(Mock.Book25()));

        BookDTO createdBook = gson.fromJson(b.andReturn().getResponse().getContentAsString(), BookDTO.class);

        ResultActions c = this.mvc.perform(get("/api/books/" + createdBook.getId()))
                .andExpect(status().isOk());
        c = checkJson(c, expect(Mock.Book25()));

        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "gino wickman"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author358()).insidePageContent().atPosition(0));

        ResultActions f = this.mvc.perform(get("/api/authors")
                .param("query", "onion person"))
                .andExpect(status().isOk());
        f = checkJson(f, expect(Mock.Author708()).insidePageContent().atPosition(0));

        ResultActions d = this.mvc.perform(delete("/api/books/" + createdBook.getId()))
                .andExpect(status().isNoContent());

        ResultActions e = this.mvc.perform(get("/api/books/" + createdBook.getId()))
                .andExpect(status().isNotFound());

        ResultActions g = this.mvc.perform(get("/api/authors")
                .param("query", "gino wickman"))
                .andExpect(status().isOk());
        g = checkJson(g, expect(Mock.Author358()).insidePageContent().atPosition(0));
    }

    @Test
    public void removeNonExistentTest() throws Exception {
        ResultActions a = this.mvc.perform(delete("/api/books/4678898"))
                .andExpect(status().isNotFound());
    }

}
