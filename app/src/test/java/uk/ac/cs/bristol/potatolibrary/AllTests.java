package uk.ac.cs.bristol.potatolibrary;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import uk.ac.cs.bristol.potatolibrary.controllers.CopyControllerLendingUnitTests;
import uk.ac.cs.bristol.potatolibrary.controllers.EmailParamVerificationUnitTests;
import uk.ac.cs.bristol.potatolibrary.tests.*;


/**
 * Runs all the tests. Currently they require 'Testcontainers' framework which instantiates a MySQL database inside Docker
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({

        // Optional email send tests
        EmailTests.class,

        // Unit Tests
        EmailParamVerificationUnitTests.class,
        CopyControllerLendingUnitTests.class,

        // Integration Tests
        MockAuthorTests.class,
        MockBookTests.class,
        MockCopyTests.class,
        MockLendingTests.class,
        MockSearchTests.class,
        MockBookSortTests.class,
        MockCopySortTests.class,
        MockRequestTests.class,
        MockReviewTests.class
})

public class AllTests {
}
