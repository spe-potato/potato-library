package uk.ac.cs.bristol.potatolibrary.tests;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockSearchTests {

    @Autowired
    private MockMvc mvc;

    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    // TODO: 04/11/18 Search by author tests

    @Test
    public void singleWordTitleExactMatchSearchTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "Phasellus"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book3()).insidePageContent().atPosition(0));
    }

    @Test
    public void singleWordTitleLowerCaseSearchTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "phasellus")) // Actual title is Phasellus
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book3()).insidePageContent().atPosition(0));
    }

    @Test
    public void sentenceTitleExactMatchSearchTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "Winter in Paradise"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(0));
    }

    @Test
    public void singleWordTitlePartialMatchWithTwoResultsSearchTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "potatoes"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(ImmutableList.of(Mock.Book15(), Mock.Book16())).insidePageContent());
    }

    @Test
    public void fullNameSearchTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "Stephen J. Dubner"))
//                .param("sort", "")
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book7()).insidePageContent().atPosition(1));
        a = checkJson(a, expect(Mock.Book11()).insidePageContent().atPosition(2));
        a = checkJson(a, expect(Mock.Book15()).insidePageContent().atPosition(3));
        a = checkJson(a, expect(Mock.Book19()).insidePageContent().atPosition(4));
        // TODO: 29/01/19 Implement backend book sorting to get rid of slightly unpredictable behaviour
    }

    @Test
    public void isbnExactMatchSearchTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "9780062300017"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Book17()).insidePageContent().atPosition(0));
    }

    @Test
    public void searchAfterBookAddedTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "9780062300024"))
                .andExpect(contentSize(0));

        ResultActions b = this.mvc.perform(post("/api/books")
                .content(gson.toJson(Mock.Book24()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
        b = checkJson(b, expect(Mock.Book24()));

        ResultActions c = this.mvc.perform(get("/api/books")
                .param("query", "9780062300024"))
                .andExpect(status().isOk());
        c = checkJson(c, expect(Mock.Book24()).insidePageContent().atPosition(0));

    }

    @Test
    public void onlyOneBookReturnedWhenMultipleAuthorsTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "Nulla interdum"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        a = checkJson(a, expect(Mock.Book7()).insidePageContent().atPosition(0));
    }
}
