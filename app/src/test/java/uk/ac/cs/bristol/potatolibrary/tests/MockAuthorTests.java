package uk.ac.cs.bristol.potatolibrary.tests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.checkJson;
import static uk.ac.cs.bristol.potatolibrary.Tester.expect;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;


/**
 * Tests all the API methods provided under /books
 * These are all end-to-end integration tests as explained in docs/Testing.md
 */

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockAuthorTests {

    @Autowired
    private MockMvc mvc;

    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void getAuthor357ById() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/authors/357"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author357()));
    }

    @Test
    public void getAuthorByFullFirstName() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "friedrich"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author357()).insidePageContent().atPosition(0));
    }

    @Test
    public void getAuthorByPartialFirstName() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "Fried"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author357()).insidePageContent().atPosition(0));
    }

    @Test
    public void getAuthorByFullLastName() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "dürrenmatt"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author357()).insidePageContent().atPosition(0));
    }

    @Test
    public void getAuthorByPartialLastName() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "Dürren"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author357()).insidePageContent().atPosition(0));
    }

    @Test
    public void getAuthorByFullName() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/authors")
                .param("query", "friedrich dürrenmatt"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Author357()).insidePageContent().atPosition(0));
    }

    // Author creation tests are in Mock Book Tests!

}
