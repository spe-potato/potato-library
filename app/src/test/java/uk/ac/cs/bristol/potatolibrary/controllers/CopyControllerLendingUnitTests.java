package uk.ac.cs.bristol.potatolibrary.controllers;

import org.junit.Test;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.CopyDTO;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

public class CopyControllerLendingUnitTests {

    private CopyController copyController = new CopyController(null, null);

    @Test
    public void checkOutTest1() {
        checkOutACopyTest(Mock.Copy3());
    }

    @Test
    public void checkOutTest2() {
        checkOutACopyTest(Mock.Copy5());
    }

    @Test
    public void checkOutTest3() {
        checkOutACopyTest(Mock.Copy8());
    }

    @Test
    public void checkOutTest4() {
        checkOutACopyTest(Mock.Copy22());
    }

    @Test
    public void checkInTest1() throws Exception {
        checkInACopyTest(Mock.Copy3());
    }

    @Test
    public void checkInTest2() throws Exception {
        checkInACopyTest(Mock.Copy5());
    }

    @Test
    public void checkInTest3() throws Exception {
        checkInACopyTest(Mock.Copy8());
    }

    @Test
    public void checkInTest4() throws Exception {
        checkInACopyTest(Mock.Copy22());
    }

    private void checkOutACopyTest(CopyDTO copyDTO) {
//        copyDTO.setBook(new BookDTO(0L, 0L, "", new HashSet<>(), "", "", 0L));
        Copy copy = new Copy(copyDTO);

        copyController.checkOutACopy(
                LocalDateTime.of(
                        2018,
                        12,
                        11,
                        13,
                        4,
                        5
                ),
                copy,
                "test@example.com"
        );

        assertNotNull("Lending shouldn't be null", copy.getLendings().get(0));
        assertNull("Lending should have null check in date", copy.getLendings().get(0).getCheckInDate());
        assertEquals("Check out date should match", LocalDateTime.of(2018, 12, 11, 13, 4, 5), copy.getLendings().get(0).getCheckOutDate());
        assertEquals("Email should match", "test@example.com", copy.getLendings().get(0).getEmail());
    }

    private void checkInACopyTest(CopyDTO copyDTO) throws Exception {
//        copyDTO.setBook(new BookDTO(0L, 0L, "", new HashSet<>(), "", "", 0L));
        Copy copy = new Copy(copyDTO);

        copyController.checkOutACopy(
                LocalDateTime.of(
                        2018,
                        12,
                        11,
                        13,
                        4,
                        5
                ),
                copy,
                "test@example.com"
        );

        copyController.checkInACopy(
                LocalDateTime.of(
                        2018,
                        12,
                        11,
                        13,
                        14,
                        0
                ),
                copy
        );

        assertNotNull("Lending shouldn't be null", copy.getLendings().get(0));
        assertNotNull("Lending shouldn't have null check in date", copy.getLendings().get(0).getCheckInDate());
        assertEquals("Check out date should match", LocalDateTime.of(2018, 12, 11, 13, 4, 5), copy.getLendings().get(0).getCheckOutDate());
        assertEquals("Check in date should match", LocalDateTime.of(2018, 12, 11, 13, 14, 0), copy.getLendings().get(0).getCheckInDate());
        assertEquals("Email should match", "test@example.com", copy.getLendings().get(0).getEmail());

    }
}
