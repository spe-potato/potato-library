package uk.ac.cs.bristol.potatolibrary.tests;

import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import uk.ac.cs.bristol.potatolibrary.Tester;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.RequestDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Status;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockRequestTests {

    @Autowired
    private MockMvc mvc;

    private final Gson gson = Tester.getGson();

    // -----------------------------------------------------------------------------------------------------------------
    // --- TESTS
    // -----------------------------------------------------------------------------------------------------------------

    @Test
    public void getRequest12ByRequestID() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests/12"))
                .andExpect(status().isOk());

        r = checkJson(r, expect(Mock.Request12()));
    }

    @Test
    public void getRequest13ByRequestID() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Request13()));
    }


    @Test
    public void getRequestByIdNotFound() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests/999"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getRequestFilteredByBookId() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("bookid", "1"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request13()).insidePageContent().atPosition(0));
    }

    @Test
    public void getRequestFilteredByLocation() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("location", "bristol"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request12()).insidePageContent().atPosition(0));
    }

    @Test
    public void getRequestFilteredByStatus() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("status", "created"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request13()).insidePageContent().atPosition(0));
    }

    @Test
    public void searchRequestsByTitle() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("query", "History of potatoes"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request12()).insidePageContent().atPosition(0));
    }

    @Test
    public void searchRequestsByTitleAndFilterByLocation() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("query", "History of potatoes")
                .param("location", "bristol"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request12()).insidePageContent().atPosition(0));
    }

    @Test
    public void searchRequestsByTitleAndFilterByWrongLocation() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("query", "History of potatoes")
                .param("location", "london"))
                .andExpect(status().isOk())
                .andExpect(contentSize(0));
    }

    @Test
    public void searchRequestsByUserEmail() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("query", "potatolibrary@gmail.com"))
                .andExpect(status().isOk())
                .andExpect(contentSize(2));

        r = checkJson(r, expect(ImmutableList.of(Mock.Request12(), Mock.Request13())).insidePageContent());
    }

    @Test
    public void searchRequestsByAdminEmail() throws Exception {

        ResultActions a = this.mvc.perform(put("/api/requests/12/delegate")
                .param("email", "newadmin@example.com"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request12NewAdmin()));

        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("query", "newadmin@example.com"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request12NewAdmin()).insidePageContent().atPosition(0));
    }

    @Test
    public void searchRequestsByIsbn() throws Exception {
        ResultActions r = this.mvc.perform(get("/api/requests")
                .param("query", "9780062300015"))
                .andExpect(status().isOk())
                .andExpect(contentSize(1));

        r = checkJson(r, expect(Mock.Request12()).insidePageContent().atPosition(0));
    }

    @Test
    public void addRequest() throws Exception {

        LocalDateTime beforeApiCall = LocalDateTime.now();

        ResultActions a = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request14()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        a = checkJson(a, expect(Mock.Request14Created()));

        RequestDTO createdRequest = gson.fromJson(a.andReturn().getResponse().getContentAsString(), RequestDTO.class);

        ResultActions b = this.mvc.perform(get("/api/requests/" + createdRequest.getId()))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Request14Created()));

        assertNotNull("Request ID was null", createdRequest.getId());
        assertNotNull("Date Created was null", createdRequest.getDateCreated());
        assertTrue("Date created was too early", createdRequest.getDateCreated().compareTo(beforeApiCall) > 0);
        assertEquals("Status was incorrect", Status.created, createdRequest.getStatus());

        ResultActions c = this.mvc.perform(delete("/api/requests/" + createdRequest.getId()))
                .andExpect(status().isNoContent());

        ResultActions d = this.mvc.perform(get("/api/requests/" + createdRequest.getId()))
                .andExpect(status().isNotFound());

    }

    @Test
    public void removeNonExistentRequest() throws Exception {
        ResultActions a = this.mvc.perform(delete("/api/requests/4678898"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateStatus() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request13()));

        ResultActions b = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "ordered"))
                .andExpect(status().isOk());

        b = checkJson(b, expect(Mock.Request13Ordered()));

        ResultActions c = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        c = checkJson(c, expect(Mock.Request13Ordered()));
    }

    @Test
    public void updateStatusNotFound() throws Exception {
        ResultActions b = this.mvc.perform(put("/api/requests/99/status")
                .param("notify", "false")
                .param("status", "ordered"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void singleCopyCreatedWhenRequestCompleted() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request13()));

        ResultActions b = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isOk());

        b = checkJson(b, expect(Mock.Request13Completed()));

        ResultActions c = this.mvc.perform(get("/api/copies")
                .param("bookid", "1")
                .param("location", "london"))
                .andExpect(status().isOk())
                .andExpect(size(1));

        c = checkJson(c, expect(Mock.CopyFromRequest13()).atPosition(0));

    }

    @Test
    public void singleCopyCreatedAndNotifiedWhenRequestCompleted() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request13()));

        ResultActions b = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "true")
                .param("status", "completed"))
                .andExpect(status().isOk());

        b = checkJson(b, expect(Mock.Request13Completed()));

        ResultActions c = this.mvc.perform(get("/api/copies")
                .param("bookid", "1")
                .param("location", "london"))
                .andExpect(status().isOk())
                .andExpect(size(1));

        c = checkJson(c, expect(Mock.CopyFromRequest13()).atPosition(0));

    }

    @Test
    public void singleCopyWithBadBookIdWhenRequestCompleted() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request13()));

        this.mvc.perform(delete("/api/books/1"))
                .andExpect(status().isNoContent());

        ResultActions b = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void multipleCopiesCreatedWhenRequestCompleted() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/12"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request12()));

        ResultActions d = this.mvc.perform(get("/api/copies")
                .param("bookid", "15")
                .param("location", "london"))
                // And implicit sorting by copy id
                .andExpect(status().isOk())
                .andExpect(size(1));

        d = checkJson(d, expect(Mock.Copy17()).atPosition(0));

        ResultActions b = this.mvc.perform(put("/api/requests/12/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isOk());

        b = checkJson(b, expect(Mock.Request12Completed()));

        ResultActions c = this.mvc.perform(get("/api/copies")
                .param("bookid", "15"))
                // And implicit sorting by copy id
                .andExpect(status().isOk())
                .andExpect(size(4));

        c = checkJson(c, expect(Mock.Copy17()).atPosition(0));
        c = checkJson(c, expect(Mock.CopyFromRequest12()).atPosition(1));
        c = checkJson(c, expect(Mock.CopyFromRequest12()).atPosition(2));
        c = checkJson(c, expect(Mock.CopyFromRequest12()).atPosition(3));

        ResultActions e = this.mvc.perform(get("/api/copies")
                .param("bookid", "15")
                .param("location", "bristol"))
                // And implicit sorting by copy id
                .andExpect(status().isOk())
                .andExpect(size(3));

        e = checkJson(e, expect(Mock.CopyFromRequest12()).atPosition(0));
        e = checkJson(e, expect(Mock.CopyFromRequest12()).atPosition(1));
        e = checkJson(e, expect(Mock.CopyFromRequest12()).atPosition(2));


    }

    @Test
    public void addRequestWrongBookId() throws Exception {

        LocalDateTime beforeApiCall = LocalDateTime.now();

        ResultActions a = this.mvc.perform(
                post("/api/requests")
                        .content(gson.toJson(Mock.RequestWithWrongBookId()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void addRequestForNewBook() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("query", "9780062300024"))
                .andExpect(contentSize(0));

        ResultActions b = this.mvc.perform(post("/api/books")
                .content(gson.toJson(Mock.Book24()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());
        b = checkJson(b, expect(Mock.Book24()));

        BookDTO createdBook = gson.fromJson(b.andReturn().getResponse().getContentAsString(), BookDTO.class);
        Long bookId = Objects.requireNonNull(createdBook.getId());

        ResultActions c = this.mvc.perform(get("/api/books")
                .param("query", "9780062300024"))
                .andExpect(status().isOk());
        c = checkJson(c, expect(Mock.Book24()).insidePageContent().atPosition(0));

        ResultActions d = this.mvc.perform(get("/api/requests/15"))
                .andExpect(status().isNotFound());

        LocalDateTime beforeApiCall = LocalDateTime.now();

        ResultActions e = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request15(bookId)))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        e = checkJson(e, expect(Mock.Request15Created(bookId)));

        RequestDTO createdRequest = gson.fromJson(e.andReturn().getResponse().getContentAsString(), RequestDTO.class);

        ResultActions f = this.mvc.perform(get("/api/requests/" + createdRequest.getId()))
                .andExpect(status().isOk());
        f = checkJson(f, expect(Mock.Request15Created(bookId)));

        assertNotNull("Request ID was null", createdRequest.getId());
        assertNotNull("Date Created was null", createdRequest.getDateCreated());
        assertTrue("Date created was too early", createdRequest.getDateCreated().compareTo(beforeApiCall) > 0);
        assertEquals("Status was incorrect", Status.created, createdRequest.getStatus());

        ResultActions g = this.mvc.perform(delete("/api/requests/" + createdRequest.getId()))
                .andExpect(status().isNoContent());

        ResultActions h = this.mvc.perform(get("/api/requests/" + createdRequest.getId()))
                .andExpect(status().isNotFound());

    }

    @Test
    public void updateAdminEmail() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/requests/12/delegate")
                .param("email", "newadmin@example.com"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request12NewAdmin()));
    }

    @Test
    public void updateAdminEmailWrongEmail() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/requests/12/delegate")
                .param("email", "newadminexample.com"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateAdminEmailWrongRequest() throws Exception {
        ResultActions a = this.mvc.perform(put("/api/requests/123/delegate")
                .param("email", "newadmin@example.com"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateStatusToCompletedWhenAlreadyCompleted() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request13()));

        ResultActions b = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isOk());

        b = checkJson(b, expect(Mock.Request13Completed()));

        ResultActions c = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        c = checkJson(c, expect(Mock.Request13Completed()));

        ResultActions d = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void updateStatusToOrderedWhenAlreadyCompleted() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        a = checkJson(a, expect(Mock.Request13()));

        ResultActions b = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isOk());

        b = checkJson(b, expect(Mock.Request13Completed()));

        ResultActions c = this.mvc.perform(get("/api/requests/13"))
                .andExpect(status().isOk());

        c = checkJson(c, expect(Mock.Request13Completed()));

        ResultActions d = this.mvc.perform(put("/api/requests/13/status")
                .param("notify", "false")
                .param("status", "ordered"))
                .andExpect(status().isBadRequest());

    }

    // TODO: 29/01/19 Sorting the requests

    @Test
    public void deleteCompletedAndRejected() throws Exception {

        ResultActions a = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request14()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        a = checkJson(a, expect(Mock.Request14Created()));

        RequestDTO createdRequest1 = gson.fromJson(a.andReturn().getResponse().getContentAsString(), RequestDTO.class);

        ResultActions b = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request14()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        b = checkJson(b, expect(Mock.Request14Created()));

        RequestDTO createdRequest2 = gson.fromJson(b.andReturn().getResponse().getContentAsString(), RequestDTO.class);

        ResultActions c = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request14()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        c = checkJson(c, expect(Mock.Request14Created()));

        RequestDTO createdRequest3 = gson.fromJson(c.andReturn().getResponse().getContentAsString(), RequestDTO.class);

        ResultActions d = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request14()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        d = checkJson(d, expect(Mock.Request14Created()));

        RequestDTO createdRequest4 = gson.fromJson(d.andReturn().getResponse().getContentAsString(), RequestDTO.class);

        ResultActions e = this.mvc.perform(post("/api/requests")
                .content(gson.toJson(Mock.Request14()))
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated());

        e = checkJson(e, expect(Mock.Request14Created()));

        RequestDTO createdRequest5 = gson.fromJson(e.andReturn().getResponse().getContentAsString(), RequestDTO.class);


        this.mvc.perform(put("/api/requests/" + createdRequest2.getId() + "/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isOk());

        this.mvc.perform(put("/api/requests/" + createdRequest4.getId() + "/status")
                .param("notify", "false")
                .param("status", "completed"))
                .andExpect(status().isOk());

        this.mvc.perform(put("/api/requests/" + createdRequest3.getId() + "/status")
                .param("notify", "false")
                .param("status", "rejected"))
                .andExpect(status().isOk());

        this.mvc.perform(put("/api/requests/" + createdRequest5.getId() + "/status")
                .param("notify", "false")
                .param("status", "ordered"))
                .andExpect(status().isOk());

        this.mvc.perform(delete("/api/requests"))
                .andExpect(status().isNoContent());

        this.mvc.perform(get("/api/requests/" + createdRequest2.getId()))
                .andExpect(status().isNotFound());

        this.mvc.perform(get("/api/requests/" + createdRequest3.getId()))
                .andExpect(status().isNotFound());

        this.mvc.perform(get("/api/requests/" + createdRequest4.getId()))
                .andExpect(status().isNotFound());

        ResultActions i = this.mvc.perform(get("/api/requests/" + createdRequest1.getId()))
                .andExpect(status().isOk());

        i = checkJson(i, expect(Mock.Request14Created()));

        ResultActions j = this.mvc.perform(get("/api/requests/" + createdRequest5.getId()))
                .andExpect(status().isOk());

        j = checkJson(j, expect(Mock.Request14Ordered()));

    }


}
