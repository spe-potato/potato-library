package uk.ac.cs.bristol.potatolibrary.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static uk.ac.cs.bristol.potatolibrary.Tester.*;
import static uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port = 0"})
@AutoConfigureMockMvc
@Transactional
public class MockBookSortTests {

    @Autowired
    private MockMvc mvc;


    @Test
    public void bookSortByIdAscTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("sortby", "book_id"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));

        a = checkJson(a, expect(Mock.Book1()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(1));
    }

    @Test
    public void bookSortByIdDescTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("sortby", "book_id")
                .param("sortdirection", "DESC"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));

        a = checkJson(a, expect(Mock.Book23()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book21()).insidePageContent().atPosition(1));
    }

    @Test
    public void bookSortByTitleAscTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("sortby", "title"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));

        a = checkJson(a, expect(Mock.Book8()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book12()).insidePageContent().atPosition(1));
    }

    @Test
    public void bookSortByTitleDescTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("sortby", "title")
                .param("sortdirection", "DESC"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));

        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book10()).insidePageContent().atPosition(1));
    }

    @Test
    public void bookPopularitySortingTest() throws Exception {
        ResultActions a = this.mvc.perform(get("/api/books")
                .param("sortby", "popularity")
                .param("sortdirection", "DESC"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));

//        a = checkJson(a, expect(ImmutableList.of(Mock.Book2(), Mock.Book3(), Mock.Book6(), Mock.Book12())).insidePageContent());
        a = checkJson(a, expect(Mock.Book2()).insidePageContent().atPosition(0));
        a = checkJson(a, expect(Mock.Book3()).insidePageContent().atPosition(1));
        a = checkJson(a, expect(Mock.Book6()).insidePageContent().atPosition(2));
        a = checkJson(a, expect(Mock.Book12()).insidePageContent().atPosition(3));
    }

    @Test
    public void bookPopularityAfterLendingSortingTest() throws Exception {

        ResultActions a = this.mvc.perform(put("/api/copies/31/check_in")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        a = checkJson(a, expect(Mock.Copy31()).available());

        ResultActions b = this.mvc.perform(get("/api/copies/31")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        b = checkJson(b, expect(Mock.Copy31()).available());

        ResultActions x = this.mvc.perform(put("/api/copies/31/check_out")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        x = checkJson(x, expect(Mock.Copy31Popularity2()).notAvailable());

        ResultActions r = this.mvc.perform(get("/api/copies/31")
                .param("email", "test@example.com"))
                .andExpect(status().isOk());
        r = checkJson(r, expect(Mock.Copy31Popularity2()).notAvailable());

        ResultActions c = this.mvc.perform(get("/api/books")
                .param("sortby", "popularity")
                .param("sortdirection", "DESC"))
                .andExpect(status().isOk())
                .andExpect(contentSize(20));

        c = checkJson(c, expect(Mock.Book12Popularity2()).insidePageContent().atPosition(0));
    }

}
