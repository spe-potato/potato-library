package uk.ac.cs.bristol.potatolibrary.testdata;

import org.jetbrains.annotations.NotNull;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class TestCases {

    // -----------------------------------------------------------------------------------------------------------------

    public static class Mock {

        public static BookDTO Book1() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    1L,
                    9780062300001L,
                    "The Clockmaker's Daughter",
                    authors,
                    "Maecenas",
                    "Sed",
                    0L
            );

        }

        public static BookDTO Book1Popularity1() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    1L,
                    9780062300001L,
                    "The Clockmaker's Daughter",
                    authors,
                    "Maecenas",
                    "Sed",
                    1L
            );

        }

        public static BookDTO Book2() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1024());

            return new BookDTO(
                    2L,
                    9780062300002L,
                    "Winter in Paradise",
                    authors,
                    "Suspendisse",
                    "potenti",
                    1L
            );

        }

        public static BookDTO Book3() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1023());

            return new BookDTO(
                    3L,
                    9780062300003L,
                    "Phasellus",
                    authors,
                    "elementum sapien",
                    "Interdum",
                    1L
            );

        }

        public static BookDTO Book3Popularity2() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1023());

            return new BookDTO(
                    3L,
                    9780062300003L,
                    "Phasellus",
                    authors,
                    "elementum sapien",
                    "Interdum",
                    2L
            );

        }

        public static BookDTO Book3Popularity3() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1023());

            return new BookDTO(
                    3L,
                    9780062300003L,
                    "Phasellus",
                    authors,
                    "elementum sapien",
                    "Interdum",
                    3L
            );

        }

        public static BookDTO Book3Popularity4() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1023());

            return new BookDTO(
                    3L,
                    9780062300003L,
                    "Phasellus",
                    authors,
                    "elementum sapien",
                    "Interdum",
                    4L
            );

        }

        public static BookDTO Book4() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1214());

            return new BookDTO(
                    4L,
                    9780062300004L,
                    "The Subtle Art of Not Giving a F*ck",
                    authors,
                    "malesuada primis",
                    "https://images.gr-assets.com/books/1465761302l/28257707.jpg",
                    0L
            );

        }

        public static BookDTO Book5() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author861());

            return new BookDTO(
                    5L,
                    9780062300005L,
                    "Ut nec eros fringilla!",
                    authors,
                    "sollicitudin",
                    "Fusce feugiat",
                    0L
            );

        }

        public static BookDTO Book6() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author358());

            return new BookDTO(
                    6L,
                    9780062300006L,
                    "Quisque et massa dapibus",
                    authors,
                    "neque vel",
                    "Aliquam erat volutpat.",
                    1L
            );

        }

        public static BookDTO Book7() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());
            authors.add(Author1024());
            authors.add(Author861());

            return new BookDTO(
                    7L,
                    9780062300007L,
                    "Nulla interdum",
                    authors,
                    "tincidunt",
                    "euismod",
                    0L
            );

        }

        public static BookDTO Book8() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    8L,
                    9780062300008L,
                    "Aenean",
                    authors,
                    "Proin ac feugiat leo. Sed aliquam erat ipsum, eu viverra magna faucibus atPosition.",
                    "In auctor sapien elit",
                    0L
            );

        }

        public static BookDTO Book9() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    9L,
                    9780062300009L,
                    "FIRE",
                    authors,
                    "Praesent maximus felis in enim suscipit, vel interdum metus rhoncus. Suspendisse finibus vestibulum ullamcorper.",
                    "vel metus",
                    0L
            );

        }

        public static BookDTO Book10() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1023());

            return new BookDTO(
                    10L,
                    9780062300010L,
                    "Vivamus commodo",
                    authors,
                    "fermentum",
                    "Curabitur",
                    0L
            );

        }

        public static BookDTO Book11() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1024());

            return new BookDTO(
                    11L,
                    9780062300011L,
                    "bibendum sit!!!\"\\\'£$%^&*()-=_+",
                    authors,
                    "If this book fails, it's probably because of the special characters",
                    "libero?",
                    0L
            );

        }

        public static BookDTO Book12() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author358());

            return new BookDTO(
                    12L,
                    9780062300012L,
                    "BĄK",
                    authors,
                    "Spadł bąk na strąk, a strąk na pąk.",
                    "Pękł pąk, pękł strąk, a bąk się zląkł",
                    1L
            );

        }

        public static BookDTO Book12Popularity2() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author358());

            return new BookDTO(
                    12L,
                    9780062300012L,
                    "BĄK",
                    authors,
                    "Spadł bąk na strąk, a strąk na pąk.",
                    "Pękł pąk, pękł strąk, a bąk się zląkł",
                    2L
            );

        }

        public static BookDTO Book13() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    13L,
                    9780062300013L,
                    "Gabriel Narutowicz",
                    authors,
                    "Prezydent",
                    "Polski",
                    0L
            );

        }

        public static BookDTO Book14() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author861());

            return new BookDTO(
                    14L,
                    9780062300014L,
                    "Testing is boring",
                    authors,
                    "Developers hate it",
                    "",
                    0L
            );

        }

        public static BookDTO Book15() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1024());

            return new BookDTO(
                    15L,
                    9780062300015L,
                    "History of potatoes",
                    authors,
                    "The history",
                    "h",
                    0L
            );

        }

        public static BookDTO Book15WithAverageRating() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1024());

            return new BookDTO(
                    15L,
                    9780062300015L,
                    "History of potatoes",
                    authors,
                    "The history",
                    "h",
                    0L,
                    2.75D
            );

        }

        public static BookDTO Book16() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author358());

            return new BookDTO(
                    16L,
                    9780062300016L,
                    "Future of Potatoes",
                    authors,
                    "Do potatoes have a future?",
                    "f",
                    0L
            );

        }

        public static BookDTO Book17() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author358());

            return new BookDTO(
                    17L,
                    9780062300017L,
                    "idk",
                    authors,
                    "?",
                    "?!",
                    0L
            );

        }

        public static BookDTO Book18() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    18L,
                    9780062300018L,
                    "Innovation",
                    authors,
                    "",
                    "",
                    0L
            );

        }

        public static BookDTO Book19() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1024());
            authors.add(Author358());
            authors.add(Author1023());

            return new BookDTO(
                    19L,
                    9780062300019L,
                    "Ignite",
                    authors,
                    "",
                    "",
                    0L
            );

        }

        public static BookDTO Book20() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author357());

            return new BookDTO(
                    20L,
                    9780062300020L,
                    "Der Besuch der alten Dame",
                    authors,
                    "The visit",
                    "DE",
                    0L
            );
        }

        public static BookDTO Book21() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    21L,
                    9780062300021L,
                    "FIRE: How Fast, Inexpensive, Restrained, and Elegant Methods Ignite Innovation",
                    authors,
                    "",
                    "",
                    0L
            );

        }

        // Not added to the DB in the init script
        // Used to test adding a new book
        public static BookDTO Book22() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author707());

            return new BookDTO(
                    null, // Book ID gets assigned automatically
                    9780062300022L,
                    "Add book test",
                    authors,
                    "",
                    "",
                    0L
            );

        }

        public static BookDTO Book23() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author706());

            return new BookDTO(
                    23L,
                    9780062300023L,
                    "Last book",
                    authors,
                    "",
                    "",
                    0L
            );

        }

        // Not added to the DB in the init script
        // Used to test searching after a new book got added
        public static BookDTO Book24() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author1024());

            return new BookDTO(
                    null, // Book ID gets assigned automatically
                    9780062300024L,
                    "Search after book added test",
                    authors,
                    "",
                    "",
                    0L
            );
        }

        // For adding when author already exists
        public static BookDTO Book25() {

            Set<AuthorDTO> authors = new HashSet<>();
            authors.add(Author358());
            authors.add(Author708());

            return new BookDTO(
                    null, // Book ID gets assigned automatically
                    9780062300019L,
                    "One author already exists and one doesn't test",
                    authors,
                    "ghj",
                    "abc",
                    0L
            );
        }

        public static CopyDTO Copy2() {
            return new CopyDTO(
                    2L,
                    1L,
//                    null,
                    Location.bristol,
                    null,
                    Book1()
            );
        }

        public static CopyDTO Copy2Popularity1() {
            return new CopyDTO(
                    2L,
                    1L,
//                    null,
                    Location.bristol,
                    null,
                    Book1Popularity1()
            );
        }

        public static CopyDTO Copy3() {
            return new CopyDTO(
                    3L,
                    2L,
//                    null,
                    Location.london,
                    null,
                    Book2()
            );
        }

        public static CopyDTO Copy4() {
            return new CopyDTO(
                    4L,
                    2L,
//                    null,
                    Location.london,
                    null,
                    Book2()
            );
        }

        public static CopyDTO Copy5() {
            return new CopyDTO(
                    5L,
                    3L,
//                    null,
                    Location.bristol,
                    null,
                    Book3()
            );
        }

        public static CopyDTO Copy5Popularity2() {
            return new CopyDTO(
                    5L,
                    3L,
//                    null,
                    Location.bristol,
                    null,
                    Book3Popularity2()
            );
        }

        public static CopyDTO Copy5Popularity3() {
            return new CopyDTO(
                    5L,
                    3L,
//                    null,
                    Location.bristol,
                    null,
                    Book3Popularity3()
            );
        }

        public static CopyDTO Copy5Popularity4() {
            return new CopyDTO(
                    5L,
                    3L,
//                    null,
                    Location.bristol,
                    null,
                    Book3Popularity4()
            );
        }

        public static CopyDTO Copy8() {
            return new CopyDTO(
                    8L,
                    6L,
//                    null,
                    Location.london,
                    null,
                    null
            );
        }

        public static CopyDTO Copy17() {
            return new CopyDTO(
                    17L,
                    15L,
//                    Book15(),
                    Location.london,
                    null,
                    null
            );
        }

        public static CopyDTO Copy22() {
            return new CopyDTO(
                    22L,
                    20L,
//                    null,
                    Location.sf,
                    null,
                    null
            );
        }

        public static CopyDTO Copy24() {
            return new CopyDTO(
                    null,// Autogenerated
                    13L,
//                    null,
                    Location.ebook,
                    null,
                    null
            );
        }

        public static CopyDTO BadCopy24() {
            return new CopyDTO(
                    null,// Autogenerated
                    134343L,
//                    null,
                    Location.ebook,
                    null,
                    null
            );
        }

        public static CopyDTO Copy24Created() {
            return new CopyDTO(
                    null,// Autogenerated
                    13L,
//                    Book13(),
                    Location.ebook,
                    null,
                    null
            );
        }

        public static CopyDTO Copy130() {
            return new CopyDTO(
                    130L,
                    20L,
//                    null,
                    Location.bristol,
                    null,
                    null
            );
        }

        public static CopyDTO Copy2567() {
            return new CopyDTO(
                    2567L,
                    20L,
//                    null,
                    Location.london,
                    null,
                    null
            );
        }

        public static CopyDTO Copy31() {
            return new CopyDTO(
                    31L,
                    12L,
//                    Mock.Book12(),
                    Location.sf,
                    null,
                    null
            );
        }

        public static CopyDTO Copy31Popularity2() {
            return new CopyDTO(
                    31L,
                    12L,
//                    Mock.Book12Popularity2(),
                    Location.sf,
                    null,
                    null
            );
        }

        @NotNull
        public static AuthorDTO Author706() {
            return new AuthorDTO(706L, "Ward", "Dan");
        }

        @NotNull
        public static AuthorDTO Author707() {
            return new AuthorDTO(null, "Potato", "Person");
        }

        @NotNull
        public static AuthorDTO Author708() {
            return new AuthorDTO(null, "Onion", "Person");
        }

        @NotNull
        public static AuthorDTO Author1024() {
            return new AuthorDTO(1024L, "J. Dubner", "Stephen");
        }

        @NotNull
        public static AuthorDTO Author1023() {
            return new AuthorDTO(1023L, "Levitt", "Steven D.");
        }

        @NotNull
        public static AuthorDTO Author1214() {
            return new AuthorDTO(1214L, "Drucker", "Peter F.");
        }

        @NotNull
        public static AuthorDTO Author861() {
            return new AuthorDTO(861L, "Collins", "Collins");
        }

        @NotNull
        public static AuthorDTO Author358() {
            return new AuthorDTO(358L, "Wickman", "Gino");
        }

        @NotNull
        public static AuthorDTO Author357() {
            return new AuthorDTO(357L, "Dürrenmatt", "Friedrich");
        }

        // Response
        public static RequestDTO Request12() {
            return new RequestDTO(
                    12L,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book15().getId(),
                    3,
                    Location.bristol,
                    Status.ordered,
                    LocalDateTime.of(2019, 1, 20, 10, 45, 56),
                    null
            );
        }

        // Response
        public static RequestDTO Request12NewAdmin() {
            return new RequestDTO(
                    12L,
                    "potatolibrary@gmail.com",
                    "newadmin@example.com",
                    Book15().getId(),
                    3,
                    Location.bristol,
                    Status.ordered,
                    LocalDateTime.of(2019, 1, 20, 10, 45, 56),
                    null
            );
        }

        // Response
        public static RequestDTO Request12Completed() {
            return new RequestDTO(
                    12L,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book15().getId(),
                    3,
                    Location.bristol,
                    Status.completed,
                    LocalDateTime.of(2019, 1, 20, 10, 45, 56),
                    null
            );
        }

        public static CopyDTO CopyFromRequest12() {
            return new CopyDTO(
                    null,
                    15L,
//                    Book15(),
                    Location.bristol,
                    null,
                    null
            );
        }

        // Response
        public static RequestDTO Request13() {
            return new RequestDTO(
                    13L,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book1().getId(),
                    1,
                    Location.london,
                    Status.created,
                    LocalDateTime.of(2019, 1, 21, 16, 31, 50),
                    null
            );
        }

        // Response
        public static RequestDTO Request13Ordered() {
            return new RequestDTO(
                    13L,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book1().getId(),
                    1,
                    Location.london,
                    Status.ordered,
                    LocalDateTime.of(2019, 1, 21, 16, 31, 50),
                    null
            );
        }

        // Response
        public static RequestDTO Request13Completed() {
            return new RequestDTO(
                    13L,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book1().getId(),
                    1,
                    Location.london,
                    Status.completed,
                    LocalDateTime.of(2019, 1, 21, 16, 31, 50),
                    null
            );
        }

        public static CopyDTO CopyFromRequest13() {
            return new CopyDTO(
                    null,
                    1L,
//                    Book1(),
                    Location.london,
                    null,
                    null
            );
        }

        // POST Request: New request for copies of a book that already exists
        public static RequestDTO Request14() {
            return new RequestDTO(
                    null,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book12().getId(),
                    2,
                    Location.london,
                    null,
                    null,
                    null
            );
        }

        // Response
        public static RequestDTO Request14Created() {
            return new RequestDTO(
                    null,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book12().getId(),
                    2,
                    Location.london,
                    Status.created,
                    null,
                    null
            );
        }

        public static RequestDTO Request14Ordered() {
            return new RequestDTO(
                    null,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    Book12().getId(),
                    2,
                    Location.london,
                    Status.ordered,
                    null,
                    null
            );
        }

        // POST Request: New request for a book that didn't exist
        public static RequestDTO Request15(Long bookId) {
            return new RequestDTO(
                    null,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    bookId, // ID received after Book 24 got created
                    5,
                    Location.sf,
                    null,
                    null,
                    null
            );
        }

        public static RequestDTO Request15Created(Long bookId) {
            return new RequestDTO(
                    null,
                    "potatolibrary@gmail.com",
                    "potatolibrary@gmail.com",
                    bookId, // ID received after Book 24 got created
                    5,
                    Location.sf,
                    Status.created,
                    null,
                    null
            );
        }

        // POST Request: New BAD request
        public static RequestDTO RequestWithWrongBookId() {
            return new RequestDTO(
                    null,
                    "chris.gora@bristol.ac.uk",
                    "kg17815@bristol.ac.uk",
                    999L,
                    5,
                    Location.sf,
                    null,
                    null,
                    null
            );
        }

        // Response
        public static ReviewDTO Review1() {
            return new ReviewDTO(
                    1L,
                    "potatolibrary@gmail.com",
                    15L,
                    1D,
                    "Who would have thought the history of potatoes would be so dull",
                    "Poorly written, unexciting and lacks flavour, just like potatoes",
                    LocalDateTime.of(2019, 2, 3, 10, 45, 56),
                    null
            );
        }

        // Response
        public static ReviewDTO Review2() {
            return new ReviewDTO(
                    2L,
                    "potatolibrary2@gmail.com",
                    16L,
                    4.9D,
                    "Best book about potatoes this year!",
                    "A riveting whistle stop tour through the advancements of the potato industry. Covering topics such as genetic modifications and the rise of cyber spuds. Would recommend to anyone who is into potatoes",
                    LocalDateTime.of(2019, 1, 21, 16, 31, 50),
                    null
            );
        }

        public static ReviewDTO Review3() {
            return new ReviewDTO(
                    3L,
                    "potatolibrary@gmail.com",
                    15L,
                    4.5D,
                    "This book Cured my insomnia!",
                    "Unfortunately I never managed to finish this book since it put me to sleep every time I read the first page. Pretty cover though.",
                    LocalDateTime.of(2019, 2, 2, 12, 12, 36),
                    null
            );
        }

        public static ReviewDTO Review4() {
            return new ReviewDTO(
                    null,
                    "potatolibrary@gmail.com",
                    15L,
                    4.8D,
                    "Title",
                    "I got bored of coming up with original content for reviews",
                    null,
                    null
            );
        }

        public static ReviewDTO Review4Created() {
            return new ReviewDTO(
                    4L,
                    "potatolibrary@gmail.com",
                    15L,
                    4.8D,
                    "Title",
                    "I got bored of coming up with original content for reviews",
                    null,
                    null
            );
        }

    }
}
