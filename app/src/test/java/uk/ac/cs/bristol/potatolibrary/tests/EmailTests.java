package uk.ac.cs.bristol.potatolibrary.tests;

import org.junit.Test;
import uk.ac.cs.bristol.potatolibrary.controllers.EmailController;
import uk.ac.cs.bristol.potatolibrary.testdata.TestCases.Mock;


public class EmailTests {

    @Test
    public void sendTestEmail() throws Exception {
        EmailController emailController = new EmailController(null);
        emailController.sendTestEmail("potatolibrary@gmail.com");
    }

    @Test
    public void sendNewRequestEmail() throws Exception {
        EmailController emailController = new EmailController(null);
        emailController.sendNewRequestEmail("potatolibrary@gmail.com", Mock.Request13());
    }

    @Test
    public void sendRequestStatusChangeEmailOrdered() throws Exception {
        EmailController emailController = new EmailController(null);
        emailController.sendRequestStatusChangeEmail("potatolibrary@gmail.com", Mock.Request13Ordered());
    }

    @Test
    public void sendRequestStatusChangeEmailCompleted() throws Exception {
        EmailController emailController = new EmailController(null);
        emailController.sendRequestStatusChangeEmail("potatolibrary@gmail.com", Mock.Request13Completed());
    }

    @Test
    public void sendAdminChangeEmail() throws Exception {
        EmailController emailController = new EmailController(null);
        emailController.sendAdminChangeEmail("potatolibrary@gmail.com", Mock.Request12NewAdmin());
    }
}
