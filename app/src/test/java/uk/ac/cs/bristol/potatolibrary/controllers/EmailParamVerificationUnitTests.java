package uk.ac.cs.bristol.potatolibrary.controllers;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EmailParamVerificationUnitTests {

    @Test
    public void correctEmails1() {
        assertTrue(ControllerUtils.correctEmail("test@example.com"));
    }

    @Test
    public void correctEmails2() {
        assertTrue(ControllerUtils.correctEmail("abc@gmail.com"));
    }

    @Test
    public void correctEmails3() {
        assertTrue(ControllerUtils.correctEmail("a@b.com"));

    }

    @Test
    public void correctEmails4() {
        assertTrue(ControllerUtils.correctEmail("abtg@wp.pl"));
    }

    @Test
    public void correctEmails5() {
        assertTrue(ControllerUtils.correctEmail("russiag@tr.ru"));
    }

    @Test
    public void correctEmails6() {
        assertTrue(ControllerUtils.correctEmail("ab12345g@my.bristol.ac.uk"));
    }

    @Test
    public void incorrectEmails1() {
        assertFalse(ControllerUtils.correctEmail("test@example"));
    }

    @Test
    public void incorrectEmails2() {
        assertFalse(ControllerUtils.correctEmail("gmail.com"));
    }

    @Test
    public void incorrectEmails3() {
        assertFalse(ControllerUtils.correctEmail("a@b..com"));
    }

    @Test
    public void incorrectEmails4() {
        assertFalse(ControllerUtils.correctEmail("@wp.pl"));
    }

    @Test
    public void incorrectEmails5() {
        assertFalse(ControllerUtils.correctEmail("russia"));
    }

    @Test
    public void incorrectEmails6() {
        assertFalse(ControllerUtils.correctEmail("russia@"));
    }
}
