DROP TABLE IF EXISTS `author`;
CREATE TABLE `author` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `surname` varchar(255) DEFAULT NULL,
  `forenames` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1219 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `author` WRITE;
INSERT INTO `author` VALUES (358,'Wickman','Gino'),(706,'Ward','Dan'),(861,'Collins','Collins'),(1023,'Levitt','Steven D.'),(1024,'J. Dubner','Stephen'),(1214,'Drucker','Peter F.'),(357, 'Dürrenmatt','Friedrich');
UNLOCK TABLES;

DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` INT unsigned NOT NULL AUTO_INCREMENT,
  `isbn` bigint(64) unsigned NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(5000) DEFAULT NULL,
  `thumbnail_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `book` WRITE;
INSERT INTO `book` VALUES
(1, 9780062300001,'The Clockmaker''s Daughter','Maecenas','Sed'),
(2, 9780062300002,'Winter in Paradise','Suspendisse','potenti'),
(3, 9780062300003,'Phasellus','elementum sapien','Interdum'),
(4, 9780062300004,'The Subtle Art of Not Giving a F*ck','malesuada primis','https://images.gr-assets.com/books/1465761302l/28257707.jpg'),
(5, 9780062300005,'Ut nec eros fringilla!','sollicitudin','Fusce feugiat'),
(6, 9780062300006,'Quisque et massa dapibus','neque vel','Aliquam erat volutpat.'),
(7, 9780062300007,'Nulla interdum','tincidunt','euismod'),
(8, 9780062300008,'Aenean','Proin ac feugiat leo. Sed aliquam erat ipsum, eu viverra magna faucibus atPosition.','In auctor sapien elit'),
(9, 9780062300009,'FIRE','Praesent maximus felis in enim suscipit, vel interdum metus rhoncus. Suspendisse finibus vestibulum ullamcorper.','vel metus'),
(10, 9780062300010,'Vivamus commodo','fermentum','Curabitur'),
(11, 9780062300011,'bibendum sit!!!\"\\''£$%^&*()-=_+','If this book fails, it''s probably because of the special characters','libero?'),
(12, 9780062300012,'BĄK','Spadł bąk na strąk, a strąk na pąk.','Pękł pąk, pękł strąk, a bąk się zląkł'),
(13, 9780062300013,'Gabriel Narutowicz','Prezydent','Polski'),
(14, 9780062300014,'Testing is boring','Developers hate it',''),
(15, 9780062300015,'History of potatoes','The history','h'),
(16, 9780062300016,'Future of Potatoes','Do potatoes have a future?','f'),
(17, 9780062300017,'idk','?','?!'),
(18, 9780062300018,'Innovation','',''),
(19, 9780062300019,'Ignite','',''),
(20, 9780062300020,'Der Besuch der alten Dame','The visit','DE'),
(21, 9780062300021,'FIRE: How Fast, Inexpensive, Restrained, and Elegant Methods Ignite Innovation','',''),
(23, 9780062300023,'Last book','','');
UNLOCK TABLES;

DROP TABLE IF EXISTS `copy`;
CREATE TABLE `copy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location` enum('bristol','london','sf', 'ebook') NOT NULL,
  `book_id` INT unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id_idx` (`book_id`),
  CONSTRAINT `copy_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1737 DEFAULT CHARSET=utf8mb4;

LOCK TABLES `copy` WRITE;
-- INSERT INTO `copy` VALUES (2,'london',9780062300001),(3,'london',9780062300002),(5,'bristol',9780062300003),(6,'london',9780062300004),(7,'london',9780062300005),(8,'london',9780062300006),(9,'bristol',9780062300007),(10,'london',9780062300008),(11,'london',9780062300009),(12,'london',9780062300010),(13,'london',9780062300011),(14,'london',9780062300012),(15,'london',9780062300013),(16,'london',9780062300014),(17,'london',9780062300015),(18,'london',9780062300016),(19,'london',9780062300017),(20,'london',9780062300018),(21,'london',9780062300019),(22,'london',9780062300020),(23,'london',9780062300021),(24,'bristol',9780062300023),(130,'bristol',9780062300020),(2567,'london',9780062300020),(31,'sf',9780062300020);
INSERT INTO `copy` VALUES
(2,'bristol',1),
(3,'london',2),
(4,'london',2),
(5,'bristol',3),
(6,'london',4),
(7,'london',5),
(8,'london',6),
(9,'bristol',7),
(1,'london',8),
(11,'london',9),
(12,'london',10),
(13,'london',11),
(14,'london',12),
(15,'london',13),
(16,'london',14),
(17,'london',15),
(18,'london',16),
(19,'london',17),
(20,'london',18),
(21,'london',19),
(22,'sf',20),
(23,'london',21),
(130,'bristol',20),
(2567,'london',20),
(31,'sf',12);
UNLOCK TABLES;

DROP TABLE IF EXISTS `author_book`;
CREATE TABLE `author_book` (
  `author_id` int(10) unsigned NOT NULL,
  `book_id` INT unsigned NOT NULL,
  PRIMARY KEY (`author_id`,`book_id`),
  KEY `book_id_idx` (`book_id`),
  CONSTRAINT `author_book_author_id` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `author_book_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `author_book` WRITE;
INSERT INTO `author_book` VALUES
(706,1),
(1024,2),
(1023,3),
(1214,4),
(861,5),
(358,6),
(706,7),
(1024,7),
(861,7),
(706,8),
(706,9),
(1023,10),
(1024,11),
(358,12),
(706,13),
(861,14),
(1024,15),
(358,16),
(358,17),
(706,18),
(1023,19),
(1024,19),
(358,19),
(357,20),
(706,21),
(706,23);
UNLOCK TABLES;

DROP TABLE IF EXISTS `lending`;
CREATE TABLE `lending` (
  `lend_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `copy_id` int(10) unsigned NOT NULL,
  `check_out_date` datetime NOT NULL,
  `check_in_date` datetime DEFAULT NULL,
  PRIMARY KEY (`lend_id`),
  KEY `copy_id_idx` (`copy_id`),
  CONSTRAINT `lending_copy_id` FOREIGN KEY (`copy_id`) REFERENCES `copy` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `lending` WRITE;
INSERT INTO `lending` VALUES
(1,'kg17815@my.bristol.ac.uk',5,'2018-09-27 11:59:02','2018-09-30 15:00:01'),
(2,'kg17815@my.bristol.ac.uk',3,'2018-09-28 18:00:45',NULL),
(3,'kg17815@my.bristol.ac.uk',8,'2018-09-30 13:59:01',NULL),
(4,'test@example.com',31,'2018-10-31 11:50:45',NULL);
UNLOCK TABLES;

DROP TABLE IF EXISTS `request`;
CREATE TABLE `request` (
  `id` INT unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `book_id` INT unsigned DEFAULT NULL,
  `quantity` INT unsigned NOT NULL,
  `location` enum('bristol','london','sf', 'ebook') NOT NULL,
  `status` enum('created','ordered','rejected', 'completed') NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id_idx` (`book_id`),
  CONSTRAINT `request_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `request` WRITE;
INSERT INTO `request` VALUES
(12,'potatolibrary@gmail.com','potatolibrary@gmail.com',15,3,'bristol','ordered','2019-01-20 10:45:56'),
(13,'potatolibrary@gmail.com','potatolibrary@gmail.com',1,1,'london','created','2019-01-21 16:31:50');
UNLOCK TABLES;


DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `id` INT unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `book_id` INT unsigned DEFAULT NULL,
  `rating` FLOAT unsigned DEFAULT NULL,
  `title` VARCHAR(255) DEFAULT NULL,
  `content` VARCHAR(4000) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id_idx` (`book_id`),
  CONSTRAINT `review_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `review` WRITE;
INSERT INTO `review` VALUES
(1,'potatolibrary@gmail.com',15,1,'Who would have thought the history of potatoes would be so dull','Poorly written, unexciting and lacks flavour, just like potatoes','2019-02-03 10:45:56'),
(2,'potatolibrary2@gmail.com',16,4.9,'Best book about potatoes this year!','A riveting whistle stop tour through the advancements of the potato industry. Covering topics such as genetic modifications and the rise of cyber spuds. Would recommend to anyone who is into potatoes','2019-01-21 16:31:50'),
(3,'potatolibrary@gmail.com',15,4.5,'This book Cured my insomnia!','Unfortunately I never managed to finish this book since it put me to sleep every time I read the first page. Pretty cover though.','2019-02-02 12:12:36');
UNLOCK TABLES;
