package uk.ac.cs.bristol.potatolibrary.entities;

import lombok.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.CopyDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/*
Database entity class for copies
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(exclude = "book")
@ToString(exclude = "book")
public class Copy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    @Enumerated(EnumType.STRING)
    private Location location;

    @OrderBy("lend_id desc")
    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "copy",
            cascade = CascadeType.ALL
    )
    private List<Lending> lendings = new ArrayList<>();

    public Copy(CopyDTO copyDTO) {
        this(
                copyDTO.getId(),
                null,
                copyDTO.getLocation(),
                new ArrayList<>()
        );
    }
}
