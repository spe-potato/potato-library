package uk.ac.cs.bristol.potatolibrary.controllers;

import com.google.common.annotations.VisibleForTesting;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Availability;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.CopyDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;
import uk.ac.cs.bristol.potatolibrary.entities.Lending;
import uk.ac.cs.bristol.potatolibrary.repositories.CopyRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.CopySpecifications;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.SpecificationHelper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The handlers for each of the requests concerning copies. This gives the API access to all
 * the copy objects. It allows them to be read and updated.
 */
@Controller
@RequestMapping("/api/copies")
public class CopyController {

    private final CopyRepository copyRepository;
    private final BookController bookController;

    @Autowired
    public CopyController(CopyRepository copyRepository, BookController bookController) {
        this.copyRepository = copyRepository;
        this.bookController = bookController;
    }

    /**
     * GET copy HTTP request handler
     * Not currently in use.
     *
     * @param id book lendId from
     *
     * @return A successful HTTP response if copy exists and copy DTO
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<CopyDTO> getCopyById(@PathVariable Long id) {
        return ControllerUtils.get(id, copyRepository, CopyDTO::new);
    }

    /**
     * Creates a list of all the copies given a GET copies by ISBN request
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<List<CopyDTO>> getCopies(
            @RequestParam(defaultValue = "20") Integer limit,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(value = "bookid", required = false) Long bookId,
            @RequestParam(value = "sortby", defaultValue = "copy_id") String sortBy,
            @RequestParam(defaultValue = "anywhere") Location location,
            @RequestParam(defaultValue = "all") Availability availability,
            @RequestParam(required = false) String email,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection) {

        Specification<Copy> spec = SpecificationHelper.conjunction();
        spec = spec.and(CopySpecifications.sortId(sortDirection.isAscending()));

        if (bookId != null)
            spec = spec.and(CopySpecifications.filterBookId(bookId));
        if (location != Location.anywhere)
            spec = spec.and(CopySpecifications.filterLocation(location));
        if (availability != Availability.all)
            spec = spec.and(CopySpecifications.filterAvailability(availability));
        if (email != null)
            spec = spec.and(CopySpecifications.filterEmail(email));
        if (sortBy.equalsIgnoreCase("checkout_date")) {
            spec = spec.and(CopySpecifications.sortCheckoutDate(sortDirection.isAscending()));
        }

        List<CopyDTO> copies;

        copies = copyRepository.findAll(spec)
                .stream()
                .map(CopyDTO::new)
                .collect(Collectors.toList());

        return ResponseEntity.ok(copies);
    }

    /**
     * The POST copy request handler. Creates new copy and saves it to the JPA repository
     *
     * @param copyDTO
     *
     * @return Success Response.
     */
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<CopyDTO>> postCopy(
            @RequestBody CopyDTO copyDTO,
            @RequestParam(defaultValue = "1") Integer quantity) {

        BookDTO associatedBook = bookController.getBookById(copyDTO.getBookId()).getBody();
        if (associatedBook == null) {
            return ResponseEntity.badRequest().build();
        }

        // FIXME: 05/02/19 Lookup the book id to make sure it's correct before creating a copy

        List<CopyDTO> createdCopies = new ArrayList<>();

        for (int i = 0; i < quantity; i++) {
            Copy copy = new Copy(copyDTO);
            copy.setBook(new Book(associatedBook));
            copy = copyRepository.save(copy);
            createdCopies.add(new CopyDTO(copy));
        }

        return new ResponseEntity<>(createdCopies, HttpStatus.CREATED);
    }

    /**
     * Remove a copy from the database
     *
     * @param id id of the copy to be removed
     *
     * @return Success HTTP response if deleted
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCopy(@PathVariable("id") Long id) {
        return ControllerUtils.delete(id, copyRepository);
    }

    /**
     * The borrowing request handler. changes the status of the lending table using the
     * jpa repository depending on the input ans weather or not the book is on loan/returned
     * already
     *
     * @param id      the copy lendId
     * @param lending the action the requester wishes to make "check_out" or "check_in"
     *
     * @return response entity, either a bad request response if the desired action is impossible
     *         or successful with the book DTO
     */
    @PutMapping(
            value = "/{id}/{lending}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<CopyDTO> borrowReturn(
            @PathVariable("id") Long id,
            @PathVariable("lending") String lending,
            @RequestParam("email") String email) {

        if (!ControllerUtils.correctEmail(email)) {
            return ResponseEntity.badRequest().build();
        }

        LocalDateTime currentTime = LocalDateTime.now(); // gets current time to add to the lending table
        Optional<Copy> optionalCopy = copyRepository.findById(id);
        Copy copy;
        if (optionalCopy.isPresent()) { // make sure copy exists
            System.out.println(lending);
            copy = optionalCopy.get();
        } else return ResponseEntity.notFound().build();

        Boolean available = copy.getLendings().size() == 0 || copy.getLendings().get(0).getCheckInDate() != null;

        if (available && lending.equals("check_out")) { // make sure copy can be taken put if that's what is requested
            checkOutACopy(currentTime, copy, email);
            copy = copyRepository.save(copy);
            return ResponseEntity.ok(new CopyDTO(copy));

        } else {
            try {
                if (!available && lending.equals("check_in") && getMostRecentLending(copy).getEmail().equals(email)) {
                    checkInACopy(currentTime, copy);
                    copy = copyRepository.save(copy);
                    return ResponseEntity.ok(new CopyDTO(copy));

                } else {
                    return ResponseEntity.badRequest().build();
                }
            } catch (Exception e) {
                return ResponseEntity.badRequest().build();
            }
        }
    }

    /**
     * clean short function that adds a new lending and updates the copy
     *
     * @param currentTime
     * @param copy
     *
     * @return the response of saving the copy
     */
    @VisibleForTesting
    void checkOutACopy(LocalDateTime currentTime, Copy copy, String email) {
        Lending lending = new Lending();
        lending.setEmail(email);
        lending.setCheckOutDate(currentTime);
        lending.setCopy(copy);

        copy.getLendings().add(0, lending);
    }

    /**
     * clean short  function that updates the most resent lending to have a checkin date and saves the
     * copy and lending table
     *
     * @param currentTime
     * @param copy
     *
     * @return the Copy result of ssaving the copy to the CopyRepository
     */
    @VisibleForTesting
    void checkInACopy(LocalDateTime currentTime, Copy copy) throws Exception {
        Lending lending = getMostRecentLending(copy);
        lending.setCheckInDate(currentTime);
    }

    @NotNull
    private Lending getMostRecentLending(Copy copy) throws Exception {
        Optional<Lending> optionalLending = copy
                .getLendings()
                .stream()
                .max(Comparator.comparing(Lending::getLendId));
        if (optionalLending.isPresent()) {
            return optionalLending.get();
        } else {
            throw new Exception("No lendings for the copy");
        }
    }

}
