package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import uk.ac.cs.bristol.potatolibrary.entities.Author;

//Author Data Transfer Object
@Data
@AllArgsConstructor
@ApiModel(value = "Author", description = "An author object.")
public class AuthorDTO {

    private final Long id;
    private final String lastName;
    private final String firstName;

    /**
     * Author DTO constructor
     *
     * @param a Author object that it constructs the DTO from
     */
    public AuthorDTO(Author a) {
        this(
                a.getId(),
                a.getSurname(),
                a.getForenames()
        );
    }

}
