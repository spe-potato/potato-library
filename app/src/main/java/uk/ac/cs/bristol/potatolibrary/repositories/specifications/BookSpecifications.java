package uk.ac.cs.bristol.potatolibrary.repositories.specifications;

import org.hibernate.query.criteria.internal.expression.ExpressionImpl;
import org.springframework.data.jpa.domain.Specification;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.entities.Author;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;
import uk.ac.cs.bristol.potatolibrary.entities.Lending;

import javax.persistence.criteria.*;

public class BookSpecifications {

    /**
     * Gives the specification providing the predicate used to search
     * for the given string.
     *
     * @param query The query string to search by. Will get a result if:
     *              <ul>
     *              <li>query is IN title</li>
     *              <li>query in IN author surname or forenames</li>
     *              <li>query IS isbn</li>
     *              </ul>
     *
     * @return The specification to be passed to the repository method
     */
    public static Specification<Book> search(String query) {
        return (book, criteriaQuery, criteriaBuilder) -> {
            SetJoin<Book, Author> authors = book.joinSet("authors", JoinType.LEFT);

            Predicate predicate = criteriaBuilder.or(
                    criteriaBuilder.like(book.get("title"), "%" + query + "%"),
                    criteriaBuilder.like(authors.get("surname"), "%" + query + "%"),
                    criteriaBuilder.like(authors.get("forenames"), "%" + query + "%")
            );

            if (isIsbn(query)) {
                // append ISBN search predicate if the query is a 13 digit number
                predicate = criteriaBuilder.or(
                        predicate,
                        criteriaBuilder.equal(book.get("isbn"), query)
                );
            }

            // TODO: Better way of doing this?
            criteriaQuery.distinct(true);

            return predicate;
        };
    }

    /**
     * @param location The location to filter by. Must be EQUAL
     *
     * @return The specification for this location filter
     */
    public static Specification<Book> filterLocation(Location location) {
        return (book, criteriaQuery, criteriaBuilder) -> {
            SetJoin<Book, Copy> copy = book.joinSet("copies", JoinType.LEFT);

            criteriaQuery.distinct(true);

            return criteriaBuilder.equal(copy.get("location"), location);
        };
    }

    private static Boolean isIsbn(String string) {
        // all chars are digits and string is 13 chars long (IBSN13)
        return string.chars().allMatch(Character::isDigit) && string.length() == 13;
    }

    public static Specification<Book> sortPopularity(Boolean ascending) {
        return (book, criteriaQuery, criteriaBuilder) -> {
            SetJoin<Book, Copy> copy = book.joinSet("copies", JoinType.LEFT);
            ListJoin<Copy, Lending> lending = copy.joinList("lendings", JoinType.LEFT);
            criteriaQuery.groupBy(book.get("id"));

            Expression count = criteriaBuilder.count(lending.get("lendId"));
            Order order = ascending ? criteriaBuilder.asc(count) : criteriaBuilder.desc(count);

            criteriaQuery.orderBy(order, criteriaBuilder.asc(book.get("id")));
            return criteriaBuilder.conjunction();
        };
    }
}
