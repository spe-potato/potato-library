package uk.ac.cs.bristol.potatolibrary.entities;

import lombok.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.LendingDTO;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Database entity class for lending table
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(exclude = "copy")
@ToString(exclude = "copy")
public class Lending {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long lendId;
    private String email;

    private LocalDateTime checkOutDate;
    private LocalDateTime checkInDate;

    @ManyToOne // this is where the magic Spring JPA does it's stuff
    @JoinColumn(name = "copy_id", nullable = false)
    private Copy copy;

    public Lending(LendingDTO lendingDTO) {
        this(
                lendingDTO.getId(),
                lendingDTO.getEmail(),
                lendingDTO.getCheckOutDate(),
                lendingDTO.getCheckInDate(),
                null
        );
    }
}
