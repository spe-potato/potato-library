package uk.ac.cs.bristol.potatolibrary.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.entities.Author;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.repositories.AuthorRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.BookRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.BookSpecifications;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.SpecificationHelper;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * The main processes for controlling the books in the database, reading and updating.
 */
@Controller
@RequestMapping("/api/books")
public class BookController {

    private final BookRepository bookRepository;

    private final AuthorRepository authorRepository;

    @Autowired
    public BookController(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }


    /**
     * GET /books REST binding
     *
     * @param limit    the number of books to be returned
     * @param page     the page of books to fetch (<code>offset=page*limit</code>.)
     *                 Pages begin at 1
     * @param location if specified, only books with at least one copy in the given location will be returned
     *
     * @return Page with a list of DTOs as the content
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<BookDTO>> getBooks(
            @RequestParam(defaultValue = "20") Integer limit,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "anywhere") Location location,
            @RequestParam(defaultValue = "") String query,
            @RequestParam(defaultValue = "book_id", value = "sortby") String sortBy,
            @RequestParam(defaultValue = "ASC", value = "sortdirection") Sort.Direction sortDirection) {

        // TODO: 30/01/19 Sorting by multiple values

        Sort sort = new Sort(sortDirection, "id");
        if (sortBy.equalsIgnoreCase("title"))
            sort = new Sort(sortDirection, "title");




        Specification<Book> spec = SpecificationHelper.conjunction();

        if (!query.isEmpty()) {
            String[] words = query.split(" ");
            for (String s : words) {
                spec = spec.and(BookSpecifications.search(s));
            }
        }

        if (location != Location.anywhere) spec = spec.and(BookSpecifications.filterLocation(location));


        Pageable pageable = ControllerUtils.newPageable(page, limit, sort);
        if (sortBy.equalsIgnoreCase("popularity")) {
            spec = spec.and(BookSpecifications.sortPopularity(sortDirection.isAscending()));
            pageable = ControllerUtils.newPageable(page, limit);
        }

        Page<Book> books = bookRepository.findAll(spec, pageable);

        return ResponseEntity.ok(
                books.map(BookDTO::new)
        );
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BookDTO> getBookById(
            @PathVariable("id") Long id) {
        return ControllerUtils.get(id, bookRepository, BookDTO::new);
    }

    /**
     * Adds a book to the database from POST book HTTP request
     *
     * @param bookDTO Bolok to be added
     *
     * @return Success HTTP response
     */
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<BookDTO> postBook(@RequestBody BookDTO bookDTO) {
        Book book = new Book(bookDTO);

        Set<Author> newAuthors = new HashSet<>();

        for (Author a : book.getAuthors()) {
            Optional<Author> existingAuthor = authorRepository.findByForenamesAndSurname(a.getForenames(), a.getSurname());
            if (existingAuthor.isPresent()) {
                newAuthors.add(existingAuthor.get());
            } else {
                a = authorRepository.save(a);
                newAuthors.add(a);
            }
        }

        book.setAuthors(newAuthors);
        book = bookRepository.save(book); // saves the new created BookDTO to the JPA crud repository.

        return new ResponseEntity<>(new BookDTO(book), HttpStatus.CREATED); //build httpresonse with body as the BookDTO
    }

    /**
     * Remove a book from the database
     *
     * @param id id of the book to be removed
     *
     * @return Success HTTP response if deleted
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteBook(@PathVariable("id") Long id) {
        return ControllerUtils.delete(id, bookRepository);
    }

}
