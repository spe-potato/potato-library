package uk.ac.cs.bristol.potatolibrary.controllers;


import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;


class ControllerUtils {

    static Pageable newPageable(Integer page, Integer limit) {
        if (limit == 0) limit = Integer.MAX_VALUE;
        return PageRequest.of(page - 1, limit);
    }

    static Pageable newPageable(Integer page, Integer limit, Sort sort) {
        if (limit == 0) limit = Integer.MAX_VALUE;
        return PageRequest.of(page - 1, limit, sort);
    }

    static <E> ResponseEntity delete(Long id, CrudRepository<E, Long> repository) {
        Optional<E> toDelete = repository.findById(id);

        if (toDelete.isPresent()) {
            repository.delete(toDelete.get());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    static <E, D> ResponseEntity<D> get(Long id, CrudRepository<E, Long> repository, DTOConstructor<E, D> d) {
        Optional<E> entity = repository.findById(id);
        if (entity.isPresent()) return ResponseEntity.ok(d.construct(entity.get()));
        else return ResponseEntity.notFound().build();

    }

    interface DTOConstructor<E, D> {
        D construct(E e);
    }

    static Boolean correctEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

}
