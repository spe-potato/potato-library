package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import uk.ac.cs.bristol.potatolibrary.entities.Review;

import java.time.LocalDateTime;

// Request Data Transfer Object
@Data
@AllArgsConstructor
@ApiModel(value = "book", description = "A book object.")
public class ReviewDTO {

    private final Long id;
    private final String email;
    private final Long bookId;
    private Double rating;
    private String title;
    private String content;
    private LocalDateTime dateCreated;

    private final BookDTO bookDTO;

    /**
     * RequestDTO constructor from the Request object
     *
     * @param r Rquest object that it constructs the DTO from.
     */
    public ReviewDTO(Review r) {
        this(
                r.getId(),
                r.getEmail(),
                r.getBook().getId(),
                r.getRating(),
                r.getTitle(),
                r.getContent(),
                r.getDateCreated(),
                new BookDTO(r.getBook())
        );
    }


}
