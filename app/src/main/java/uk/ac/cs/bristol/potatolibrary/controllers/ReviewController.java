package uk.ac.cs.bristol.potatolibrary.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.ReviewDTO;
import uk.ac.cs.bristol.potatolibrary.entities.Review;
import uk.ac.cs.bristol.potatolibrary.repositories.ReviewRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.ReviewSpecifications;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.SpecificationHelper;

import java.time.LocalDateTime;

/**
 * The handlers for book and copy reviews. This gives the API access to all
 * the copy objects. It allows them to be read and updated.
 */
@SuppressWarnings("OptionalIsPresent")
@Controller
@RequestMapping("/api/reviews")
public class ReviewController {

    private final ReviewRepository reviewRepository;
    private final BookController bookController;
    private final CopyController copyController;
    private final EmailController emailController;

    @Autowired
    public ReviewController(ReviewRepository reviewRepository, BookController bookController, CopyController copyController, EmailController emailController) {
        this.reviewRepository = reviewRepository;
        this.bookController = bookController;
        this.copyController = copyController;
        this.emailController = emailController;
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<ReviewDTO> getReviewById(@PathVariable Long id) {
        return ControllerUtils.get(id, reviewRepository, ReviewDTO::new);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<ReviewDTO>> getReviews(
            @RequestParam(defaultValue = "20") Integer limit,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(value = "sortby", defaultValue = "dateCreated") String sortBy,
            @RequestParam(defaultValue = "ASC") Sort.Direction sortDirection,
            @RequestParam(value = "bookid", required = false) Long bookId,
            @RequestParam(value = "email", required = false) String email) {

        Sort sort = new Sort(sortDirection, "dateCreated");
        if (sortBy.equalsIgnoreCase("rating"))
            sort = new Sort(sortDirection, "rating");

        Pageable pageable = ControllerUtils.newPageable(page, limit, sort);
        Specification<Review> spec = SpecificationHelper.conjunction();

        if (email != null) spec = spec.and(ReviewSpecifications.filterEmail(email));
        if (bookId != null) spec = spec.and(ReviewSpecifications.filterBookId(bookId));

        Page<Review> reviews = reviewRepository.findAll(spec, pageable);

        return ResponseEntity.ok(
                reviews.map(ReviewDTO::new)
        );

    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ReviewDTO> postReview(@RequestBody ReviewDTO reviewDTO) {
        BookDTO book = bookController.getBookById(reviewDTO.getBookId()).getBody();
        if (book == null) {
            return ResponseEntity.badRequest().build();
        } else {
            reviewDTO.setDateCreated(LocalDateTime.now());
            Review review = new Review(reviewDTO);
            review = reviewRepository.save(review);

            return new ResponseEntity<>(new ReviewDTO(review), HttpStatus.CREATED);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteReview(@PathVariable("id") Long id) {
        return ControllerUtils.delete(id, reviewRepository);
    }

}
