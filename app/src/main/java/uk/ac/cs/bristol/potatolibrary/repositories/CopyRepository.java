package uk.ac.cs.bristol.potatolibrary.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;

// Crud repository that stores and updates copies
public interface CopyRepository extends PagingAndSortingRepository<Copy, Long>, JpaSpecificationExecutor<Copy> {
}
