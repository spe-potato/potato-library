package uk.ac.cs.bristol.potatolibrary.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Status;
import uk.ac.cs.bristol.potatolibrary.entities.Author;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Request;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.SetJoin;

public class RequestSpecifications {
    public static Specification<Request> search(String query) {
        return (request, criteriaQuery, criteriaBuilder) -> {

            Join<Request, Book> book = request.join("book", JoinType.LEFT);
            SetJoin<Book, Author> authors = book.joinSet("authors", JoinType.LEFT);

            Predicate predicate = criteriaBuilder.or(
                    criteriaBuilder.like(book.get("title"), "%" + query + "%"),
                    criteriaBuilder.like(authors.get("surname"), "%" + query + "%"),
                    criteriaBuilder.like(authors.get("forenames"), "%" + query + "%"),
                    criteriaBuilder.like(request.get("email"), "%" + query + "%"),
                    criteriaBuilder.like(request.get("adminEmail"), "%" + query + "%")
            );

            // TODO: 22/01/19 Refactor to remove a lot of duplicate code between this and Book Spec.

            if (isIsbn(query)) {
                // append ISBN search predicate if the query is a 13 digit number
                predicate = criteriaBuilder.or(
                        predicate,
                        criteriaBuilder.equal(book.get("isbn"), query)
                );
            }

            // TODO: Better way of doing this?
            criteriaQuery.distinct(true);

            return predicate;
        };
    }


    public static Specification<Request> filterBookId(Long id) {
        return (request, criteriaQuery, criteriaBuilder) -> {
            Join<Request, Book> books = request.join("book", JoinType.LEFT);
            return criteriaBuilder.equal(books.get("id"), id);
        };
    }

    public static Specification<Request> filterLocation(Location location) {
        return (request, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(request.get("location"), location);
    }

    public static Specification<Request> filterStatus(Status status) {
        return (request, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(request.get("status"), status);
    }

    private static Boolean isIsbn(String string) {
        // all chars are digits and string is 13 chars long (IBSN13)
        return string.chars().allMatch(Character::isDigit) && string.length() == 13;
    }
}
