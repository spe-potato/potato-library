package uk.ac.cs.bristol.potatolibrary.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.ReviewDTO;

import javax.persistence.*;
import java.time.LocalDateTime;

/*
Database entity class for reviews
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;
    private Double rating;
    private String title;
    private String content;
    private LocalDateTime dateCreated;

    public Review(ReviewDTO reviewDTO) {
        this(
                reviewDTO.getId(),
                reviewDTO.getEmail(),
                new Book(reviewDTO.getBookId()),
                reviewDTO.getRating(),
                reviewDTO.getTitle(),
                reviewDTO.getContent(),
                reviewDTO.getDateCreated()
        );
    }
}
