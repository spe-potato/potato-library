package uk.ac.cs.bristol.potatolibrary.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.AuthorDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Database entity class for author table
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String surname;

    private String forenames;

    public Author(AuthorDTO authorDTO) {
        this(authorDTO.getId(), authorDTO.getLastName(), authorDTO.getFirstName());
    }


    /**
     * @return The author's full name. This is not a field in the database, rather a combination of
     *         forenames and surname.
     */
    public String getFullName() {
        return forenames + " " + surname;
    }
}
