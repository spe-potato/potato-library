package uk.ac.cs.bristol.potatolibrary.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import uk.ac.cs.bristol.potatolibrary.entities.Review;

// Crud repository that stores and updates book requests
public interface ReviewRepository extends PagingAndSortingRepository<Review, Long>, JpaSpecificationExecutor<Review> {
}
