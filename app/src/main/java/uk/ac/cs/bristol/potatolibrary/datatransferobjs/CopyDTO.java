package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;

// Copy Data Transfer Object
@Data
@RequiredArgsConstructor
@ApiModel(value = "Copy", description = "A copy object.")
public class CopyDTO {

    private final Long id;
    private final Long bookId;
    private final Location location;
    private final LendingDTO lending;
    private final BookDTO book;

    @JsonIgnore
    private Long copyPopularity;

    /**
     * CopyDTO constructor from Copy object
     *
     * @param c Copy object that it constructs the DTO from.
     */
    public CopyDTO(Copy c) {
        this(
                c.getId(),
                c.getBook().getId(),
                c.getLocation(),
                c.getLendings().size() == 0 ? null : new LendingDTO(c.getLendings().get(0)),
                new BookDTO(c.getBook())
        );

        copyPopularity = (long) c.getLendings().size();

    }

}
