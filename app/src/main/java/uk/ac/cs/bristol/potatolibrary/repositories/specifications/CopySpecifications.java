package uk.ac.cs.bristol.potatolibrary.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Availability;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;
import uk.ac.cs.bristol.potatolibrary.entities.Lending;

import javax.persistence.criteria.*;

public class CopySpecifications {
    public static Specification<Copy> filterBookId(Long isbn) {
        return (copy, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(copy.join("book").get("id"), isbn);
    }

    public static Specification<Copy> filterLocation(Location location) {
        return (copy, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(copy.get("location"), location);
    }

    public static Specification<Copy> filterEmail(String email) {
        return (copy, criteriaQuery, criteriaBuilder) -> {
            ListJoin<Copy, Lending> lending = copy.joinList("lendings");

            criteriaQuery.distinct(false);

            return criteriaBuilder.equal(lending.get("email"), email);
        };
    }

    public static Specification<Copy> filterAvailability(Availability availability) {
        return (copy, criteriaQuery, criteriaBuilder) -> {
            ListJoin<Copy, Lending> lending = copy.joinList("lendings", JoinType.LEFT);

            Subquery<Long> subQuery = criteriaQuery.subquery(Long.class);
            Root<Lending> lendingRoot = subQuery.from(Lending.class);
            subQuery.select(criteriaBuilder.max(lendingRoot.get("lendId")));
            subQuery.where(criteriaBuilder.equal(lendingRoot.join("copy").get("id"), copy.get("id")));

            if (availability == Availability.available) {
                return criteriaBuilder.or(
                        criteriaBuilder.and(
                                criteriaBuilder.isNotNull(lending.get("checkInDate")),
                                criteriaBuilder.equal(subQuery, lending.get("lendId"))
                        ),
                        criteriaBuilder.isEmpty(copy.get("lendings"))
                );
            } else {
                return criteriaBuilder.and(
                        criteriaBuilder.isNull(lending.get("checkInDate")),
                        criteriaBuilder.equal(subQuery, lending.get("lendId"))
                );
            }
        };
    }

    public static Specification<Copy> sortCheckoutDate(Boolean ascending) {
        return (copy, criteriaQuery, criteriaBuilder) -> {
            ListJoin<Copy, Lending> lending = copy.joinList("lendings", JoinType.LEFT);

            Expression max = criteriaBuilder.max(lending.get("checkOutDate"));
            Order sort = ascending ? criteriaBuilder.asc(max) : criteriaBuilder.desc(max);

            criteriaQuery.orderBy(sort, criteriaBuilder.asc(copy.get("id")));
            criteriaQuery.groupBy(copy.get("id"));

            return criteriaBuilder.conjunction();
        };
    }

    public static Specification<Copy> sortId(Boolean ascending) {
        return (copy, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.groupBy(copy.get("id"));
            return criteriaBuilder.conjunction();
        };
    }

}
