package uk.ac.cs.bristol.potatolibrary.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Request;
import uk.ac.cs.bristol.potatolibrary.entities.Review;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

public class ReviewSpecifications {

    public static Specification<Review> filterBookId(Long id) {
        return (review, criteriaQuery, criteriaBuilder) -> {
            Join<Request, Book> books = review.join("book", JoinType.LEFT);
            return criteriaBuilder.equal(books.get("id"), id);
        };
    }

    public static Specification<Review> filterEmail(String email) {
        return (review, criteriaQuery, criteriaBuilder) -> {
            return criteriaBuilder.equal(review.get("email"), email);
        };
    }
}
