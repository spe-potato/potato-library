package uk.ac.cs.bristol.potatolibrary.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;

public class SpecificationHelper {
    public static <T> Specification<T> conjunction() {
        return (book, criteriaQuery, criteriaBuilder) -> criteriaBuilder.conjunction();
    }
}
