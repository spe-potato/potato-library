package uk.ac.cs.bristol.potatolibrary.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import uk.ac.cs.bristol.potatolibrary.entities.Book;

//Paging and sorting repository of books(clever crud repository that stores and updates books) be warned JPA magic.
public interface BookRepository extends PagingAndSortingRepository<Book, Long>, JpaSpecificationExecutor<Book> {
}
