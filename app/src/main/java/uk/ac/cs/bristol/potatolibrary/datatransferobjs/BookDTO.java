package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;
import uk.ac.cs.bristol.potatolibrary.entities.Review;

import java.util.Set;
import java.util.stream.Collectors;

// Book Data Transfer Object
@Data
@ApiModel(value = "Book", description = "A book object.")
@RequiredArgsConstructor
public class BookDTO {

    private final Long id;
    private final Long isbn;
    private final String title;
    private final Set<AuthorDTO> authors;
    private final String description;
    private final String thumbnailUrl;
    private Long popularity;
    private Double averageRating;

    /**
     * BookDTO constructor from book object
     *
     * @param b Book object that it constructs the DTO from
     */
    public BookDTO(Book b) {

        this.id = b.getId();
        this.isbn = b.getIsbn();
        this.title = b.getTitle();
        this.authors = b.getAuthors().stream().map(AuthorDTO::new).collect(Collectors.toSet());
        this.description = b.getDescription();
        this.thumbnailUrl = b.getThumbnailUrl();

        Set<Copy> copies = b.getCopies();
        this.popularity = 0L;
        for (Copy c : copies) {
            popularity = popularity + c.getLendings().size();
        }

        Set<Review> reviews = b.getReviews();
        this.averageRating = 0D;
        for (Review r : reviews) {
            averageRating = averageRating + r.getRating();
        }

        averageRating = averageRating / reviews.size();

    }

    public BookDTO(Long id, Long isbn, String title, Set<AuthorDTO> authors, String description, String thumbnailUrl, Long popularity) {
        this.id = id;
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
        this.popularity = popularity;
    }

    public BookDTO(Long id, Long isbn, String title, Set<AuthorDTO> authors, String description, String thumbnailUrl, Long popularity, Double averageRating) {
        this.id = id;
        this.isbn = isbn;
        this.title = title;
        this.authors = authors;
        this.description = description;
        this.thumbnailUrl = thumbnailUrl;
        this.popularity = popularity;
        this.averageRating = averageRating;
    }

}
