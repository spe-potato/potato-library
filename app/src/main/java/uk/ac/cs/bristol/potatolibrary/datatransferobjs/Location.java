package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

//location enum
public enum Location {

    bristol, london, sf, ebook, anywhere

}
