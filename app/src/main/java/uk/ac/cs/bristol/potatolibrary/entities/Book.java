package uk.ac.cs.bristol.potatolibrary.entities;

import lombok.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/*
Database entity class for Book table
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode
@ToString(exclude = "copies")
//@Builder
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long isbn;

    private String title;

    private String description;

    private String thumbnailUrl;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "author_book",
            joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")}
    )
    private Set<Author> authors = new HashSet<>();

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "book"
    )
    @EqualsAndHashCode.Exclude
    private Set<Copy> copies = new HashSet<>();

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "book"
    )
    @EqualsAndHashCode.Exclude
    private Set<Request> requests = new HashSet<>();

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "book"
    )
    @EqualsAndHashCode.Exclude
    private Set<Review> reviews = new HashSet<>();

    public Book(BookDTO bookDTO) {
        this(
                bookDTO.getId(),
                bookDTO.getIsbn(),
                bookDTO.getTitle(),
                bookDTO.getDescription(),
                bookDTO.getThumbnailUrl(),
                bookDTO.getAuthors().stream().map(Author::new).collect(Collectors.toSet()),
                new HashSet<>(),
                new HashSet<>(),
                new HashSet<>()
        );
    }

    public Book(Long id) {
        this.id = id;
    }

}
