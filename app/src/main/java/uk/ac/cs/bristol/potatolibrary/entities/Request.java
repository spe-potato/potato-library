package uk.ac.cs.bristol.potatolibrary.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.RequestDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Status;

import javax.persistence.*;
import java.time.LocalDateTime;

/*
Database entity class for requests
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String adminEmail;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    private Integer quantity;

    @Enumerated(EnumType.STRING)
    private Location location;

    @Enumerated(EnumType.STRING)
    private Status status;

    private LocalDateTime dateCreated;

    public Request(RequestDTO requestDTO) {
        this(
                requestDTO.getId(),
                requestDTO.getEmail(),
                requestDTO.getAdminEmail(),
                new Book(requestDTO.getBookId()),
                requestDTO.getQuantity(),
                requestDTO.getLocation(),
                requestDTO.getStatus(),
                requestDTO.getDateCreated()
        );
    }
}
