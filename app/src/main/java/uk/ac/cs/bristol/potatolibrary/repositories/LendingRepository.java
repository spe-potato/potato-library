package uk.ac.cs.bristol.potatolibrary.repositories;

import org.springframework.data.repository.CrudRepository;
import uk.ac.cs.bristol.potatolibrary.entities.Copy;
import uk.ac.cs.bristol.potatolibrary.entities.Lending;

import java.util.List;

//Crud repository that stores and updates lendings
public interface LendingRepository extends CrudRepository<Lending, Long> {
    //added methods(JPA magic)
    List<Lending> findAllByCopy(Copy copy);

    List<Lending> findAll();
}
