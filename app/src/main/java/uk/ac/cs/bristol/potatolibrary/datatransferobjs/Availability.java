package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

// for filtering
public enum Availability {
    all, available, unavailable
}
