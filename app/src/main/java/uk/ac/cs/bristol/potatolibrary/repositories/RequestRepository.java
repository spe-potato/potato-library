package uk.ac.cs.bristol.potatolibrary.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Status;
import uk.ac.cs.bristol.potatolibrary.entities.Request;

import java.util.List;

// Crud repository that stores and updates book requests
public interface RequestRepository extends PagingAndSortingRepository<Request, Long>, JpaSpecificationExecutor<Request> {
    List<Request> findAllByStatus(Status status);
}
