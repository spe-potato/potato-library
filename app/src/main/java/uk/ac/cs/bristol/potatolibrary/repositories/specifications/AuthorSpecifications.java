package uk.ac.cs.bristol.potatolibrary.repositories.specifications;

import org.springframework.data.jpa.domain.Specification;
import uk.ac.cs.bristol.potatolibrary.entities.Author;

import javax.persistence.criteria.Predicate;

public class AuthorSpecifications {

    public static Specification<Author> search(String query) {
        return (author, criteriaQuery, criteriaBuilder) -> {

            Predicate predicate = criteriaBuilder.or(
                    criteriaBuilder.like(author.get("surname"), "%" + query + "%"),
                    criteriaBuilder.like(author.get("forenames"), "%" + query + "%")
            );

            criteriaQuery.distinct(true);

            return predicate;
        };
    }
}
