package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import uk.ac.cs.bristol.potatolibrary.entities.Request;

import java.time.LocalDateTime;

// Request Data Transfer Object
@Data
@AllArgsConstructor
@ApiModel(value = "Copy", description = "A copy object.")
public class RequestDTO {

    private final Long id;
    private final String email;
    private final String adminEmail;
    private final Long bookId;
    private final Integer quantity;
    private final Location location;
    private Status status;
    private LocalDateTime dateCreated;

    private final BookDTO book;

    /**
     * RequestDTO constructor from the Request object
     *
     * @param r Request object that it constructs the DTO from.
     */
    public RequestDTO(Request r) {
        this(
                r.getId(),
                r.getEmail(),
                r.getAdminEmail(),
                r.getBook().getId(),
                r.getQuantity(),
                r.getLocation(),
                r.getStatus(),
                r.getDateCreated(),
                new BookDTO(r.getBook())
        );
    }


}
