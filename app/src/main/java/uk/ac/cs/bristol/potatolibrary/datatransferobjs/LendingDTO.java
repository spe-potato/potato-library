package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import uk.ac.cs.bristol.potatolibrary.entities.Lending;

import java.time.LocalDateTime;

// Lending Data Transfer Object
@Data
@AllArgsConstructor
@ApiModel(value = "Lending", description = "A lending object.")
public class LendingDTO {

    private final Long id;
    private final String email;
    private final LocalDateTime checkInDate;
    private final LocalDateTime checkOutDate;

    /**
     * Lending DTO constructor
     *
     * @param lending Lending object that it constructs the DTO from
     */
    public LendingDTO(Lending lending) {
        this(
                lending.getLendId(),
                lending.getEmail(),
                lending.getCheckInDate(),
                lending.getCheckOutDate()
        );
    }
}
