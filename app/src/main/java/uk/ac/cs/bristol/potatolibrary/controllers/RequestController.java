package uk.ac.cs.bristol.potatolibrary.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.CopyDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Location;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.RequestDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.Status;
import uk.ac.cs.bristol.potatolibrary.entities.Book;
import uk.ac.cs.bristol.potatolibrary.entities.Request;
import uk.ac.cs.bristol.potatolibrary.repositories.BookRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.RequestRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.RequestSpecifications;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.SpecificationHelper;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * The handlers for book and copy requests. This gives the API access to all
 * the copy objects. It allows them to be read and updated.
 */
@SuppressWarnings("OptionalIsPresent")
@Controller
@RequestMapping("/api/requests")
public class RequestController {

    private final RequestRepository requestRepository;
    private final BookController bookController;
    private final BookRepository bookRepository;
    private final CopyController copyController;
    private final EmailController emailController;

    @Autowired
    public RequestController(RequestRepository requestRepository, BookController bookController, BookRepository bookRepository, CopyController copyController, EmailController emailController) {
        this.requestRepository = requestRepository;
        this.bookController = bookController;
        this.bookRepository = bookRepository;
        this.copyController = copyController;
        this.emailController = emailController;
    }


    /**
     * GET book request HTTP request handler
     *
     * @param id request ID
     *
     * @return A successful HTTP response if the request exists and the Request DTO
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Transactional
    public ResponseEntity<RequestDTO> getRequestById(@PathVariable Long id) {
        return ControllerUtils.get(id, requestRepository, RequestDTO::new);
    }

    // TODO Continue...

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Page<RequestDTO>> getRequests(
            @RequestParam(defaultValue = "20") Integer limit,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(value = "bookid", required = false) Long bookId,
            @RequestParam(defaultValue = "anywhere") Location location,
            @RequestParam(defaultValue = "any") Status status,
            @RequestParam(defaultValue = "") String query) {

        // TODO: 26/01/19 Move to a method in some Basic Controller Class
        // TODO: 26/01/19 Make sure all params are not negative and test for this
        Pageable pageable = ControllerUtils.newPageable(page, limit);

        Specification<Request> spec = SpecificationHelper.conjunction();
        if (!query.isEmpty()) spec = spec.and(RequestSpecifications.search(query));
        if (location != Location.anywhere) spec = spec.and(RequestSpecifications.filterLocation(location));
        if (status != Status.any) spec = spec.and(RequestSpecifications.filterStatus(status));
        if (bookId != null) spec = spec.and(RequestSpecifications.filterBookId(bookId));

        Page<Request> requests = requestRepository.findAll(spec, pageable);

        return ResponseEntity.ok(
                requests.map(RequestDTO::new)
        );
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<RequestDTO> postRequest(@RequestBody RequestDTO requestDTO) {
//        BookDTO book = bookController.getBookById(requestDTO.getBookId()).getBody();
//        if (book == null) {
//            return ResponseEntity.badRequest().build();

        Optional<Book> optionalBook = bookRepository.findById(requestDTO.getBookId());
        if (!optionalBook.isPresent()) {
            return ResponseEntity.badRequest().build();
        } else {
            requestDTO.setDateCreated(LocalDateTime.now());
            requestDTO.setStatus(Status.created);

            Request request = new Request(requestDTO);
            request = requestRepository.save(request);
            requestDTO = new RequestDTO(request);

            try {
                emailController.sendNewRequestEmail(request.getEmail(), requestDTO);
                emailController.sendNewRequestEmail(request.getAdminEmail(), requestDTO);
            } catch (MessagingException e) {
                e.printStackTrace();
            }

            return new ResponseEntity<>(requestDTO, HttpStatus.CREATED);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteRequest(@PathVariable("id") Long id) {
        return ControllerUtils.delete(id, requestRepository);
    }

    @DeleteMapping
    public ResponseEntity deleteCompletedAndRejectedRequest() {
        List<Request> completed = requestRepository.findAllByStatus(Status.completed);
        for (Request r : completed) {
            requestRepository.delete(r);
        }

        List<Request> rejected = requestRepository.findAllByStatus(Status.rejected);
        for (Request r : rejected) {
            requestRepository.delete(r);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(
            value = "/{id}/status",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<RequestDTO> updateStatus(
            @PathVariable("id") Long id,
            @RequestParam(value = "notify", defaultValue = "true") Boolean notify,
            @RequestParam("status") Status newStatus) {

        Optional<Request> optionalRequest = requestRepository.findById(id);
        RequestDTO request;
        if (optionalRequest.isPresent()) { // make sure copy exists
            request = new RequestDTO(optionalRequest.get());
        } else {
            return ResponseEntity.notFound().build();
        }

        if (newStatus.getOrder() > request.getStatus().getOrder()) {
            request.setStatus(newStatus);
        } else {
            return ResponseEntity.badRequest().build();
        }


        if (newStatus == Status.completed) {

                ResponseEntity<List<CopyDTO>> responseEntity = copyController.postCopy(new CopyDTO(
                        null,
                        request.getBookId(),
//                        null,
                        request.getLocation(),
                        null,
                        null
                ), request.getQuantity());

            // TODO: 05/02/19 Refactor so that a constructor does not have to use null values (create anew copy constructor)

            if (responseEntity.getStatusCode() != HttpStatus.CREATED) {
                return ResponseEntity.badRequest().build();
            }

            if (notify) {
                try {
                    emailController.sendRequestStatusChangeEmail(request.getEmail(), request);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }

        requestRepository.save(new Request(request));
        return ResponseEntity.ok(request);

    }

    @PutMapping(
            value = "/{id}/delegate",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<RequestDTO> delegate(
            @PathVariable("id") Long id,
            @RequestParam("email") String email) {

        Optional<Request> optionalRequest = requestRepository.findById(id);
        Request request;
        if (optionalRequest.isPresent()) { // make sure copy exists
            request = optionalRequest.get();
        } else return ResponseEntity.notFound().build();

        if (!ControllerUtils.correctEmail(email)) return ResponseEntity.badRequest().build();

        request.setAdminEmail(email);

        try {
            emailController.sendAdminChangeEmail(request.getAdminEmail(), new RequestDTO(request));
        } catch (MessagingException e) {
            e.printStackTrace();
        }


        requestRepository.save(request);
        return ResponseEntity.ok(new RequestDTO(request));
    }

}
