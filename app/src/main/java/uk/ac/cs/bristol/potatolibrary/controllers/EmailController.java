package uk.ac.cs.bristol.potatolibrary.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.AuthorDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.BookDTO;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.RequestDTO;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

@Controller
public class EmailController {

    @Value("${emailcontroller.send}")
    private Boolean send;

    private final BookController bookController;

    @Autowired
    public EmailController(BookController bookController) {
        this.bookController = bookController;
    }

    public void sendTestEmail(String to) throws MessagingException {
        sendEmail(to, "Test email", "Potato Test email content. Time of the test: " + LocalDateTime.now().toString());
    }

    public void sendNewRequestEmail(String to, RequestDTO request) throws MessagingException {
        sendEmail(to, "A new Potato Library Request has been submitted", "<p>A new request has been submitted in the Potato Library:</p>" + getRequestInfo(request));
    }

    public void sendRequestStatusChangeEmail(String to, RequestDTO request) throws MessagingException {
        sendEmail(to, "Status of your Potato Library Request has changed", "<p>Your Potato Library request has been " + request.getStatus() + ". Request details are below:</p>" + getRequestInfo(request));
    }

    public void sendAdminChangeEmail(String to, RequestDTO request) throws MessagingException {
        sendEmail(to, "A Potato Library Request has been delegated to you", "<p>This email address has been set as the new admin email address responsible for the following Potato Library Request:</p>" + getRequestInfo(request));
    }

    private void sendEmail(String to, String subject, String content) throws MessagingException {
        if (!ControllerUtils.correctEmail(to)) {
            throw new MessagingException("Supplied email address was incorrect");
        }

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(System.getenv("GMAIL_LOGIN"), System.getenv("GMAIL_PASSWORD"));
            }
        });

        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(System.getenv("GMAIL_LOGIN"), false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        msg.setSubject(subject);
        msg.setContent("Hi There!" + content + "Sent to you from Potato Library!", "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent("Potato Library notification email", "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        if (send != null && send) {
            Transport.send(msg);
        } else {
            try {
                System.out.println((String) msg.getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getRequestInfo(RequestDTO request) {

        String bookDetails = "</p>";

        BookDTO book = null;
        if (bookController != null) {
            book = bookController.getBookById(request.getBookId()).getBody();
        }

        if (book != null) {
            Set<String> authors = new HashSet<>();
            for (AuthorDTO a : book.getAuthors()) {
                authors.add(a.getFirstName() + " " + a.getLastName());
            }

            bookDetails = "<p>Book ID: " + book.getId() + "<br />"
                    + "Title: " + book.getTitle() + "<br />"
                    + "Authors: " + String.join(", ", authors) + "<br />"
                    + "</p>";

        }

        return "<p> Request ID: " + request.getId() + "<br />"
                + "Submitted on: " + request.getDateCreated() + "<br />"
                + "Quantity: " + request.getQuantity() + "<br />"
                + "Location: " + request.getLocation() + "<br />"
                + "Status: " + request.getStatus() + "<br /><br />"
                + bookDetails;
    }

}
