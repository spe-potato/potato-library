package uk.ac.cs.bristol.potatolibrary.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import uk.ac.cs.bristol.potatolibrary.entities.Author;

import java.util.Optional;

// Crud repository that stores and updates authors
public interface AuthorRepository extends PagingAndSortingRepository<Author, Long>, JpaSpecificationExecutor<Author> {
    Optional<Author> findByForenamesAndSurname(String forenames, String surname);
}
