package uk.ac.cs.bristol.potatolibrary.controllers;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import uk.ac.cs.bristol.potatolibrary.datatransferobjs.AuthorDTO;
import uk.ac.cs.bristol.potatolibrary.entities.Author;
import uk.ac.cs.bristol.potatolibrary.repositories.AuthorRepository;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.AuthorSpecifications;
import uk.ac.cs.bristol.potatolibrary.repositories.specifications.SpecificationHelper;

@Controller
@RequestMapping("/api/authors")
public class AuthorController {

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    public ResponseEntity<Page<AuthorDTO>> getAuthors(
            @RequestParam(defaultValue = "20") Integer limit,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "") String query) {

        Pageable pageable = ControllerUtils.newPageable(page, limit);

        Specification<Author> spec = SpecificationHelper.conjunction();
        if (!query.isEmpty()) {

            String[] words = query.split(" ");

            for (String s : words) {
                spec = spec.and(AuthorSpecifications.search(s));
            }
        }


        Page<Author> authorsPage = authorRepository.findAll(spec, pageable);

        return ResponseEntity.ok(
                authorsPage.map(AuthorDTO::new)
        );
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<AuthorDTO> getAuthorById(
            @PathVariable("id") Long id) {
        return ControllerUtils.get(id, authorRepository, AuthorDTO::new);
    }

}
