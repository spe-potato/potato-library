package uk.ac.cs.bristol.potatolibrary.datatransferobjs;

import lombok.Getter;

// Status enum
public enum Status {

    created(0),
    ordered(1),
    rejected(2),
    completed(2),
    any(null);

    @Getter
    private Integer order;

    private Status(Integer order) {
        this.order = order;
    }
}
