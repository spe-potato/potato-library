import urllib.request
import pprint
from googleapiclient.discovery import build

service = build('books', 'v1', developerKey='AIzaSyCC6KGCjg-H0IuTZfGQWAqpI0a1QyGCsOE')
collection = service.volumes()
request = collection.list(q="isbn:9780706377453", maxResults=1)
response = request.execute()
pp = pprint.PrettyPrinter(indent=0.5)
pp.pprint(response)
