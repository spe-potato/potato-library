import pandas # Used for csv parsing
import math
import sys
import urllib.request
import json
from googleapiclient.discovery import build
import os

LIMIT = 100000
id_state = {'books_id': 849, 'author_id': 1172, 'copies_id': 932, 'reviews_id': 528}

service = build('books', 'v1', developerKey= os.environ['BOOKS_API_KEY'])
collection = service.volumes()

books = pandas.read_csv('../goodreads_library_export.csv', nrows = LIMIT, skiprows=range(1,id_state["books_id"]))

books_table = pandas.DataFrame(None, None, ["id", "isbn", "title", "description", "thumbnail_url"])

authors_table = pandas.DataFrame(None, None, ["id", "surname", "forenames"])

join_table = pandas.DataFrame(None, None, ["author_id", "book_id"])

copies_table = pandas.DataFrame(None, None, ["id", "location", "book_id"])

reviews_table = pandas.DataFrame(None, None, ["id", "email", "book_id", "rating", "title", "content", "date_created"])

# adds data about an author to the author table and join table
# returns (updated author table, updated join table)
def append_author(authors_table, join_table, surname, forenames, id):
    global id_state
    
    authors_table = authors_table.append({ "id": id_state["author_id"],
                                           "surname": surname,
                                           "forenames": forenames }, True)
    join_table = join_table.append({ "author_id": id_state["author_id"],
                                     "book_id": id }, True)
    
    id_state["author_id"] += 1

    return (authors_table, join_table)

# adds data about a book to the book table
# returns updated book table
def append_book(books_table, isbn, title, description, thumbnail_url):
    global id_state

    books_table = books_table.append({ "id": id_state["books_id"],
                                       "isbn": isbn,
                                       "title": title,
                                       "description": description,
                                       "thumbnail_url": thumbnail_url }, True)

    id_state["books_id"] += 1

    return books_table

# adds data about copies of a book to the copies table
# returns updated copies table
def append_copies(copies_table, book_id, locations_str):
    global id_state
    
    locations = locations_str.split(", ")
    for location in locations:
        if location == "e-books":
            location = "ebook"

        if location in ["london", "bristol", "ebook"]: # ignore 'to-read'
            copies_table = copies_table.append({ "id": id_state["copies_id"],
                                                 "book_id": book_id,
                                                 "location": location }, True)
            id_state["copies_id"] += 1

    return copies_table

# adds data about reviews of a book to the reviews table
#returns updated copy table
def append_review(reviews_table, email, book_id, rating, title, content):
    global id_state

    reviews_table = reviews_table.append({"id": id_state["reviews_id"],
                                          "email": email,
                                          "book_id": book_id,
                                          "rating":rating,
                                          "title": title,
                                          "content": content,
                                          "date_created": "2019-01-01 00:00:00"
                                          },True)                                    
    id_state["reviews_id"] +=1
    return reviews_table

    

# Generate one large SQL statement to import entire data set for table into db
# Will look something like:
#   INSERT INTO x (a, b)
#   VALUES
#   (1, 2),
#   (3, 5),
# etc...
#
# update_dupe - set to True if you want to generate a ON DUPLICATE KEY UPDATE clause, useful
#   if you want to update already imported data
def generate_sql(table_name, data, update_dupe):
    sql = "INSERT INTO %s (%s)\nVALUES" % (table_name, ", ".join(data.columns))

    # iterate through all data rows
    for vals in data.iterrows():

        # generate a list of the values as strings. wrap actual strings in ""
        val_list = []
        for val in vals[1]:
            if type(val) is str:
                val_list.append('"%s"' % val.replace('"','\\"'))
            else:
                val_list.append(str(val))

        # add statement to SQL
        sql += "\n(%s)," % ", ".join(val_list)

    sql = sql[:-1] # remove extra ,

    if update_dupe:
        sql += "\nON DUPLICATE KEY UPDATE"
        
        for col in data.columns[1:]:
            sql += "\n    {0} = VALUES ({0}),".format(col)

        sql = sql[:-1]

    return sql + "\n"

#Get ISBN_13 from parsed volue info
#def getIsbn13(parsed):
#    for i in range(len(parsed["industryIdentifiers"])):
#        if(parsed["industryIdentifiers"][i]["type"] == "ISBN_13"):
#           return int(parsed["industryIdentifiers"][i]["identifier"])
#    return(None)
           
# Should we build CSV or SQL output, or both?
csv = True
sql = True

for arg in sys.argv[1:]:
    if arg == "--nocsv":
        csv = False
    elif arg == "--nosql":
        sql = False

print("Generation: SQL: %s, CSV: %s." % (sql, csv))


print("Processing data...")

for vals in books.iterrows():
    title = vals[1]["Title"]
    isbn13 = vals[1]["ISBN13"][2:-1] # for some reason isbns are weirdly formatted in my csv ( ="9780712678865" )

    thumbnail_url = ""
    description = ""
    rating = None
    response = "{}"
    if(isbn13 != "" and len(str(int(isbn13))) == 13):
        request = collection.list(q=("isbn:" + str(int(isbn13))), maxResults=1)
        try:
            response = request.execute()
        except:
            print("failed request")
            break
    if(response == "{}" or response['totalItems'] == 0):
        request = collection.list(q=title , maxResults=1)
        try:
            response = request.execute()
        except:
            print("failed request")
            break
    
            
    if response != "{}":
        try:
            parsed = response['items'][0]['volumeInfo']
        except:
            print("bad response")
            print(title)
            continue
        try:
            thumbnail_url = parsed['imageLinks']['thumbnail']
        except:
            print("No image found")
        try:
            description = parsed["description"]
        except:
            print("No description found")
        try:
            rating = int(parsed['averageRating'])
        except:
            print("No rating found")

        print("Got book details! (%s, %s, %s)" % (title, thumbnail_url[:10], description[:30]))
    else:
        print("!!!!WARNING NO BOOK FOUND!!!!\n\n\n\n\n\n\n")

    # Get necessary book data and add to table
    try:
        isbn13 = int(isbn13)
    except:
        isbn13 = "NULL"
    
    new_book_id = id_state["books_id"]

    books_table = append_book(books_table, isbn13, title, description, thumbnail_url)

    # Get necessary copy data and add to table
    locations = vals[1]["Bookshelves"]

    if type(locations) is str and len(locations) > 0:
        copies_table = append_copies(copies_table, new_book_id, locations)

    # Grab necessary author data and add to tables
    author = vals[1]["Author l-f"].split(", ")
    forenames = author[1]
    surname = author[0]

    authors_table, join_table = append_author(authors_table, join_table,
                                              surname, forenames, new_book_id)


    # Process additional authors
    additional = vals[1]["Additional Authors"]

    if type(additional) is str:
        for add_author in additional.split(", "):
            # No lastname, firstname split provided - do the best we can
            split = add_author.split(" ")
            surname = " ".join(split[1:]) # not very accurate
            forenames = split[0]

            authors_table, join_table = append_author(authors_table, join_table,
                                                      surname, forenames, new_book_id)

    #Generate google review for each book
    if rating != None:
        reviews_table = append_review(reviews_table, 'Google Books', new_book_id, rating, '', '')
                             

if sql:
    # generate SQL statement for each table
    print("Generating SQL...")

    author_sql = generate_sql("author", authors_table, True)
    author_book_sql = generate_sql("author_book", join_table, False)
    book_sql = generate_sql("book", books_table, True)
    copy_sql = generate_sql("copy", copies_table, True)
    review_sql = generate_sql("copy", reviews_table, True)


    # save all SQL to files
    print("Writing SQL...")
    with open("author.sql2", "w", encoding="utf-8") as f:
        f.write(author_sql)

    with open("author_book.sql2", "w", encoding="utf-8") as f:
        f.write(author_book_sql)

    with open("book.sql2", "w", encoding="utf-8") as f:
        f.write(book_sql)

    with open("copy.sql2", "w", encoding="utf-8") as f:
        f.write(copy_sql)

    with open("review.sql2", "w", encoding="utf-8") as f:
        f.write(review_sql)
        
if csv:
    print("Writing CSV...")
    authors_table.to_csv("author.csv2", index=False)
    join_table.to_csv("author_book.csv2", index=False)
    books_table.to_csv("book.csv2", index=False)
    copies_table.to_csv("copy.csv2", index=False)
    reviews_table.to_csv("reviews.csv2", index=False)
    
    

print(id_state)
print("Done.")
