# Some ugly python to add ISBNs to all the books in our csv file

import pandas # Used for csv parsing
import requests # Used for goodreads API
import xml.etree.ElementTree as ET # Used to parse XML response

api_token = 'IhbVn1d5NtpMAIUypA0IQ'
api_url_prefix = 'https://www.goodreads.com/book/show/'
api_url_suffix = '.xml?key=' + api_token

test = True

books =  pandas.read_csv('goodreads_library_export.csv')

if test == True: ## redefine the books df so that don't work it out for all during tests
    books = books.head(2)


def get_isbn(book_id): #magic function that calls the API to get the XML
    api_url = api_url_prefix + str(book_id) + api_url_suffix
    response =requests.get(api_url) #everything about the book in xml format
    

    if response.status_code == 200: #if we get a response
        tree = ET.ElementTree(ET.fromstring(response.content))
        root = tree.getroot()
        try:
            return(int(tree.findall('book//isbn13')[0].text))
        except:
            print("failed to get ISBN")
    else:
        print('failure')

        
for i in range(0 , books.shape[0]): # goes through all columns to get ISBNs
    try:
        books.at[i,'ISBN13'] = (get_isbn(books.at[i,'Book Id']))# updates df
    except:
        print('failed to get isbn')

print(books)
books.to_csv('out.csv') # Saves df to csv
